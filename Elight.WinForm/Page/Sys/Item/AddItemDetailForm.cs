﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.Other;
using Elight.Utility.ResponseModels; 
using Elight.WinForm.Common; 
using Sunny.UI;
using System;
using System.Collections.Generic; 
using System.Drawing; 
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.Item
{
    public partial class AddItemDetailForm : UIForm
    {
        public AddItemDetailForm()
        {
            InitializeComponent();
        }


        #region 标题栏
        /// <summary>
        ///  标题
        /// </summary>
        public string Title
        {
            get
            {
                return lblTitle.Text;
            }
            set
            {
                lblTitle.Text = value;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            FormHelper.subForm = null;
            this.Close();
        }
        private Point mPoint;
        private void titlePanel_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint = new Point(e.X, e.Y);
        }

        private void titlePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.FromArgb(231, 231, 231);
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.Transparent;
        }
        #endregion
        public string Id { get; internal set; }
        public ItemPage ParentPage { get; internal set; }
        public string ItemId { get; internal set; }
        private void AddItemDetailForm_Load(object sender, EventArgs e)
        {
            if (StringHelper.IsNullOrEmpty(Id))
            {
                Text = "新增选项";
                txtEnCode.Enabled = true;
                return;
            }
            Text = "修改选项";
            txtEnCode.Enabled = false;

            //根据选项Id,得到选项明细
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("primaryKey", Id);
            string url = GlobalConfig.Url + "app/system/itemsDetail/getForm";
            string str = HttpUtils.DoPost(url, dic, 1000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后再试", UIStyle.White);
                return;
            }
            try
            {
                SysItemDetail detail = str.ToObject<SysItemDetail>();
                txtEnCode.Text = detail.EnCode;
                txtName.Text = detail.Name;
                txtSortCode.Value = detail.SortCode.Value;
            }
            catch
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后再试", UIStyle.White);
            }

        }

        /// <summary>
        /// 确定按钮事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (StringHelper.IsNullOrEmpty(Id))
            {
                DoAdd();
            }
            else
            {
                DoUpdate();
            }
        }

        /// <summary>
        /// 执行更新操作
        /// </summary>
        private void DoUpdate()
        {
            bool flag = ChechEmpty();
            if (!flag)
                return;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("Id", Id);
            parms.Add("EnCode", txtEnCode.Text);
            parms.Add("ItemId", ItemId);
            parms.Add("Name", txtName.Text);
            parms.Add("SortCode", Convert.ToString(txtSortCode.Value));
            parms.Add("ModifyUser", GlobalConfig.CurrentUser.Account);
            string url = GlobalConfig.Url + "app/system/itemsDetail/form";
            string str = HttpUtils.DoPost(url, parms, 2000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.ShowItemDetailData();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }



        /// <summary>
        /// 执行新增操作
        /// </summary>
        private void DoAdd()
        {
            bool flag = ChechEmpty();
            if (!flag)
                return;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("EnCode", txtEnCode.Text);
            parms.Add("ItemId", ItemId);
            parms.Add("Name", txtName.Text);
            parms.Add("SortCode", Convert.ToString(txtSortCode.Value));
            parms.Add("CreateUser", GlobalConfig.CurrentUser.Account);
            string url = GlobalConfig.Url + "app/system/itemsDetail/form";
            string str = HttpUtils.DoPost(url, parms, 2000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.ShowItemDetailData();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }


        /// <summary>
        /// 检验数据
        /// </summary>
        /// <returns></returns>
        private bool ChechEmpty()
        {
            if (StringHelper.IsNullOrEmpty(txtEnCode.Text))
            {
                this.ShowWarningDialog("编码不能为空", UIStyle.White);
                return false;
            }
            if (StringHelper.IsNullOrEmpty(txtName.Text))
            {
                this.ShowWarningDialog("选项名称不能为空", UIStyle.White);
                return false;
            }
            return true;
        }
    }
}
