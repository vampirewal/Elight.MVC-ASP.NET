﻿using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.Other;
using Elight.Utility.ResponseModels;
using Elight.WinForm.Common; 
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.Logs
{
    public partial class DeleteLogsForm : UIForm
    {
        public DeleteLogsForm()
        {
            InitializeComponent();
        }

        #region 标题栏
        private void btnClose_Click(object sender, EventArgs e)
        {
            FormHelper.subForm = null;
            this.Close();
        }
        private Point mPoint;
        private void titlePanel_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint = new Point(e.X, e.Y);
        }

        private void titlePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.FromArgb(231, 231, 231);
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.Transparent;
        }
        #endregion

        public UIPage ParentPage { get; set; }

        /// <summary>
        /// 确定按钮
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            //判断一下，哪个单选框被选了
            string keepType = "0";
            if (uiRadioButton1.Checked)
                keepType ="30";
            else if (uiRadioButton3.Checked)
                keepType ="7";
            else if (uiRadioButton2.Checked)
                keepType ="90";
            else if (uiRadioButton4.Checked)
                keepType ="1";
            if (keepType == "0")
            {
                this.ShowWarningDialog("请选择要删除的选项", UIStyle.White);
                return;
            }
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("keepType", keepType);
            string url = GlobalConfig.Url + "app/system/log/delete";
            string str = HttpUtils.DoPost(url, parms, 10000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.Init();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }
    }
}
