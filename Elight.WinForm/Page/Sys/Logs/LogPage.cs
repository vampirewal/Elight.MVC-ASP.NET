﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.ResponseModels;
using Elight.WinForm.Common;
using Elight.Utility.Other;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.Logs
{
    [PageCode("sys-log")]
    public partial class LogPage : MyPage
    {
        private string queryDate = "7";
        public LogPage()
        {
            InitializeComponent();
            dataGridView.AutoGenerateColumns = false;

        }

        /// <summary>
        /// 画面初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPage_Initialize(object sender, EventArgs e)
        {
            btnQuery_Click(sender, e);
        }

        /// <summary>
        /// 查询按钮事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            //调用服务器获得数据
            LayPadding<SysLog> result;

            Dictionary<string, string> parm = new Dictionary<string, string>();
            parm.Add("pageIndex", Convert.ToString(pagination.ActivePage));
            parm.Add("pageSize", Convert.ToString(pagination.PageSize));
            parm.Add("keyWord", txtKeywords.Text);
            parm.Add("queryDate", queryDate);
            string url = GlobalConfig.Url + "app/system/log/index";
            string str = HttpUtils.DoPost(url, parm, 2000);
            try
            {
                result = str.ToObject<LayPadding<SysLog>>();
            }
            catch
            {
                result = null;
            }
            if (result == null)
            {
                this.ShowInfoDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            pagination.TotalCount = (int)result.count;
            dataGridView.DataSource = result.list;
        }

        /// <summary>
        /// 关键字Enter键处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKeywords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnQuery_Click(sender, null);
        }


        /// <summary>
        /// 删除日志按钮事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteLogsForm form = new DeleteLogsForm();
            form.ParentPage = this;
            FormHelper.ShowSubForm(form);
        }

        /// <summary>
        /// 页码发生改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pagingSource"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        private void pagination_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            btnQuery_Click(null, null);
        }

        private void btnToday_Click(object sender, EventArgs e)
        {
            queryDate = "1";
        }

        private void btnSevenDay_Click(object sender, EventArgs e)
        {
            queryDate = "7";
        }

        private void btnOneMonth_Click(object sender, EventArgs e)
        {
            queryDate = "30";
        }

        private void btnThreeMonth_Click(object sender, EventArgs e)
        {
            queryDate = "90";
        }
    }
}
