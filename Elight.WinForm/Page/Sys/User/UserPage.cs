﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.ResponseModels;
using Elight.WinForm.Common;
using Elight.Utility.Other;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.User
{
    [PageCode("sys-user")]
    public partial class UserPage : MyPage
    {
        public UserPage()
        {
            InitializeComponent();
            dataGridView.AutoGenerateColumns = false;

        }

        /// <summary>
        /// 画面初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPage_Initialize(object sender, EventArgs e)
        {
            btnQuery_Click(sender, e);
        }

        /// <summary>
        /// 查询按钮事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnQuery_Click(object sender, EventArgs e)
        {
            //调用服务器获得数据
            LayPadding<SysUser> result;

            Dictionary<string, string> parm = new Dictionary<string, string>();
            parm.Add("pageIndex", Convert.ToString(pagination.ActivePage));
            parm.Add("pageSize", Convert.ToString(pagination.PageSize));
            parm.Add("keyWord", txtKeywords.Text);
            string url = GlobalConfig.Url + "app/system/user/index";
            string str = HttpUtils.DoPost(url, parm, 2000);
            try
            {
                result = str.ToObject<LayPadding<SysUser>>();
            }
            catch
            {
                result = null;
            }
            if (result == null)
            {
                this.ShowInfoDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            pagination.TotalCount = (int)result.count;
            dataGridView.DataSource = result.list;
        }

        /// <summary>
        /// 关键字Enter键处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtKeywords_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnQuery_Click(sender, null);
        }

        /// <summary>
        /// 新增用户按钮事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddUserForm form = new AddUserForm();
            form.ParentPage = this;
            form.Id = string.Empty;
            FormHelper.ShowSubForm(form);
        }

        /// <summary>
        /// 修改用户按钮事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnModify_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                this.ShowWarningDialog("请选择一行数据进行修改", UIStyle.White);
                return;
            }
            int index = dataGridView.SelectedIndex;
            if (index < 0)
            {
                this.ShowWarningDialog("请选择一行数据进行修改", UIStyle.White); return;
            }
            string id = dataGridView.Rows[index].Cells["UserId"].Value.ToString();
            AddUserForm form = new AddUserForm();
            form.ParentPage = this;
            form.Id = id;
            FormHelper.ShowSubForm(form);
        }

        /// <summary>
        /// 删除用户按钮事件处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count == 0)
            {
                this.ShowWarningDialog("请选择一行数据进行修改", UIStyle.White);
                return;
            }
            int index = dataGridView.SelectedIndex;
            if (index < 0)
            {
                this.ShowWarningDialog("请选择一行数据进行修改", UIStyle.White); return;
            }
            string id = dataGridView.Rows[index].Cells["UserId"].Value.ToString();
            if (!this.ShowAskDialog("您是否确定要删除该用户？", UIStyle.White))
            {
                return;
            }
            try
            {
                string url = GlobalConfig.Url + "app/system/user/delete";
                Dictionary<string, string> parm = new Dictionary<string, string>();
                parm.Add("userIds", id);
                parm.Add("currentUserId", GlobalConfig.CurrentUser.Id);
                string str = HttpUtils.DoPost(url, parm, 1000);
                if (StringHelper.IsNullOrEmpty(str))
                {
                    this.ShowWarningDialog("网络或服务器异常，请稍后再试", UIStyle.White);
                    return;
                }
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                //重新查询
                btnQuery_Click(null, null);
            }
            catch
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后再试", UIStyle.White);
            }
        }

        /// <summary>
        /// 页码发生改变
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="pagingSource"></param>
        /// <param name="pageIndex"></param>
        /// <param name="count"></param>
        private void pagination_PageChanged(object sender, object pagingSource, int pageIndex, int count)
        {
            btnQuery_Click(null, null);
        }
    }
}
