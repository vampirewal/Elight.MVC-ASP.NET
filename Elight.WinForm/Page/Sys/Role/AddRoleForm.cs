﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.ResponseModels;
using Elight.Utility.Security;
using Elight.WinForm.Common;
using Elight.WinForm.Properties;
using Elight.Utility.Other;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.Role
{
    public partial class AddRoleForm : UIForm
    {
        public AddRoleForm()
        {
            InitializeComponent();
        }

        #region 标题栏

        private void btnClose_Click(object sender, EventArgs e)
        {
            FormHelper.subForm = null;
            this.Close();
        }
        private Point mPoint;
        private void titlePanel_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint = new Point(e.X, e.Y);
        }

        private void titlePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.FromArgb(231, 231, 231);
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.Transparent;

        }

        #endregion

        public RolePage ParentPage { get; set; }
        public string Id { get; set; }

        /// <summary>
        /// 画面加载，读取用户信息，显示在界面上
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddRoleForm_Load(object sender, EventArgs e)
        {
            comboType.Items.Add("系统角色");
            comboType.Items.Add("业务角色");
            comboType.Items.Add("其他角色");
            comboType.SelectedIndex = 2;

            if (StringHelper.IsNullOrEmpty(Id))
            {
                lblTitle.Text = "新增角色";
                txtEnCode.Enabled = true;
            }
            else
            {
                lblTitle.Text = "修改角色";
                txtEnCode.Enabled = false;
            }
            //获取部门下拉列表的值
            bool flag = GetDepartmentCombox();
            if (!flag)
            {
                btnClose_Click(null, null);
                return;
            }
            if (StringHelper.IsNullOrEmpty(Id))
            {
                return;
            }
            Dictionary<string, string> parm = new Dictionary<string, string>();
            parm.Add("primaryKey", Id);
            //获得用户信息
            string url = GlobalConfig.Url + "app/system/role/getForm";
            string str = HttpUtils.DoPost(url, parm, 1000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog( "网络或服务器异常，请稍后重试", UIStyle.White);
                btnClose_Click(null, null);
                return;
            }
            SysRole user = null;
            try
            {
                user = str.ToObject<SysRole>();
            }
            catch
            {
                this.ShowWarningDialog( "网络或服务器异常，请稍后重试", UIStyle.White);
                btnClose_Click(null, null);
            }
            //给文本框赋值
            txtEnCode.Text = user.EnCode;
            txtName.Text = user.Name;
            comboType.SelectedIndex = user.Type.Value;
            comboDept.SelectedValue = user.OrganizeId;
            txtSortCode.Value = user.SortCode.Value;
            txtRemark.Text = user.Remark;
        }


        /// <summary>
        /// 获得组织机构下拉列表
        /// </summary>
        /// <returns></returns>
        private bool GetDepartmentCombox()
        {
            string url = GlobalConfig.Url + "System/Organize/GetListTreeSelect";
            string str = HttpUtils.DoGet(url, null, 1000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog( "网络或服务器异常，请稍后重试", UIStyle.White);
                return false;
            }
            try
            {
                List<TreeSelect> list = str.ToList<TreeSelect>();
                List<TreeSelect> list2 = list.Where(it => it.parentId != "0").ToList();
                comboDept.ValueMember = "id";
                comboDept.DisplayMember = "text";
                comboDept.DataSource = list2;
                return true;
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return false;
            }
        }

        /// <summary>
        /// 确定按钮点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (StringHelper.IsNullOrEmpty(Id))
            {
                DoAdd();
            }
            else
            {
                DoUpdate();
            }
        }

        /// <summary>
        /// 执行更新操作
        /// </summary>
        private void DoUpdate()
        {
            bool flag = ChechEmpty();
            if (!flag)
            {
                return;
            }
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("Id", Id);
            parms.Add("EnCode", txtEnCode.Text);
            parms.Add("Name", txtName.Text);
            parms.Add("Type", Convert.ToString(comboType.SelectedIndex));
            parms.Add("OrganizeId", comboDept.SelectedValue.ToString());
            parms.Add("SortCode", Convert.ToString(txtSortCode.Value));
            parms.Add("Remark", txtRemark.Text);
            parms.Add("ModifyUser", GlobalConfig.CurrentUser.Account);

            string url = GlobalConfig.Url + "app/system/role/form";
            string str = HttpUtils.DoPost(url, parms, 2000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.Init();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }

        /// <summary>
        /// 数据校验
        /// </summary>
        /// <param name="checkPassword"></param>
        /// <returns></returns>
        private bool ChechEmpty()
        {
            if (StringHelper.IsNullOrEmpty(txtEnCode.Text))
            {
                this.ShowWarningDialog("编码不能为空", UIStyle.White);
                return false;
            }
            if (StringHelper.IsNullOrEmpty(txtName.Text))
            {
                this.ShowWarningDialog("名称不能为空", UIStyle.White);
                return false;
            }
            if (StringHelper.IsNullOrEmpty(comboType.SelectedItem.ToString()))
            {
                this.ShowWarningDialog("类型不能为空", UIStyle.White);
                return false;
            }

            if (StringHelper.IsNullOrEmpty(comboDept.SelectedItem.ToString()))
            {
                this.ShowWarningDialog("所属部门不能为空", UIStyle.White);
                return false;
            }
            if (StringHelper.IsNullOrEmpty(txtSortCode.Text))
            {
                this.ShowWarningDialog("排序号不能为空", UIStyle.White);
                return false;
            }
            return true;
        }


        /// <summary>
        /// 执行新增操作
        /// </summary>
        private void DoAdd()
        {
            bool flag = ChechEmpty();
            if (!flag)
                return;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("EnCode", txtEnCode.Text);
            parms.Add("Name", txtName.Text);
            parms.Add("Type", Convert.ToString(comboType.SelectedIndex));
            parms.Add("OrganizeId", comboDept.SelectedValue.ToString());
            parms.Add("SortCode", Convert.ToString(txtSortCode.Value));
            parms.Add("Remark", txtRemark.Text);
            parms.Add("CreateUser", GlobalConfig.CurrentUser.Account);
            string url = GlobalConfig.Url + "app/system/role/form";
            string str = HttpUtils.DoPost(url, parms, 2000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.Init();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }
    }
}
