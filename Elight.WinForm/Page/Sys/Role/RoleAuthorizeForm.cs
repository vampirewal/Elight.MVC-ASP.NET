﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.ResponseModels;
using Elight.Utility.Security;
using Elight.WinForm.Common;
using Elight.WinForm.Properties;
using Elight.Utility.Other;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.Role
{
    public partial class RoleAuthorizeForm : UIForm
    {
        public RoleAuthorizeForm()
        {
            InitializeComponent();
        }
        #region 标题栏
        private void btnClose_Click(object sender, EventArgs e)
        {
            FormHelper.subForm = null;
            this.Close();
        }
        private Point mPoint;
        private void titlePanel_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint = new Point(e.X, e.Y);
        }

        private void titlePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.FromArgb(231, 231, 231);
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.Transparent;
        }
        #endregion
        public RolePage ParentPage { get; set; }
        public string Id { get; set; }

        /// <summary>
        /// 画面加载，读取用户信息，显示在界面上
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddRoleForm_Load(object sender, EventArgs e)
        {
            //根据角色Id得到角色所对应的所有权限
            Dictionary<string, string> parm = new Dictionary<string, string>();
            parm.Add("roleId", Id);
            parm.Add("operaterId", GlobalConfig.CurrentUser.Id);
            //获得用户信息
            string url = GlobalConfig.Url + "app/system/roleAuthorize/index";
            string str = HttpUtils.DoPost(url, parm, 1000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                btnClose_Click(null, null);
                return;
            }
            try
            {
                List<ZTreeNode> result = str.ToList<ZTreeNode>();
                List<ZTreeNode> fistNode = result.Where(it => it.pId == "0").ToList();
                foreach (ZTreeNode node in fistNode)
                {
                    TreeNode parentNode = new TreeNode(node.name);
                    parentNode.Tag = node.id;
                    parentNode.Checked = node.@checked;
                    //二级菜单
                    List<ZTreeNode> secondList = result.Where(it => it.pId == node.id).ToList();
                    foreach (ZTreeNode second in secondList)
                    {
                        TreeNode seconds = new TreeNode(second.name);
                        seconds.Checked = second.@checked;
                        seconds.Tag = second.id;
                        //三级菜单
                        List<ZTreeNode> thirdList = result.Where(it => it.pId == second.id).ToList();
                        foreach (ZTreeNode third in thirdList)
                        {
                            TreeNode thirds = new TreeNode(third.name);
                            thirds.Tag = third.id;
                            thirds.Checked = third.@checked;
                            seconds.Nodes.Add(thirds);
                        }
                        parentNode.Nodes.Add(seconds);
                    }
                    treeView.Nodes.Add(parentNode);
                }
                treeView.ExpandAll();
            }
            catch
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                btnClose_Click(null, null);
            }
        }


        /// <summary>
        /// 确定按钮点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            //获得所有的Tag 
            List<string> userPermissionList = new List<string>();//用于保存所有的id
            foreach (TreeNode parentNode in treeView.Nodes)
            {
                if (parentNode.Checked)
                {
                    userPermissionList.Add((string)parentNode.Tag);
                }
                //二级
                foreach (TreeNode second in parentNode.Nodes)
                {
                    if (second.Checked)
                    {
                        userPermissionList.Add((string)second.Tag);
                    }
                    //三级
                    foreach (TreeNode third in second.Nodes)
                    {
                        if (third.Checked)
                        {
                            userPermissionList.Add((string)third.Tag);
                        }
                    }
                }

            }
            Dictionary<string, string> parm = new Dictionary<string, string>();
            parm.Add("operater", GlobalConfig.CurrentUser.Account);
            parm.Add("roleId", Id);
            parm.Add("perIds", userPermissionList.GetStrArray());
            string url = GlobalConfig.Url + "app/system/roleAuthorize/form";
            string str = HttpUtils.DoPost(url, parm, 1000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                btnClose_Click(null, null);
            }
            catch
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }

        }
    }
}
