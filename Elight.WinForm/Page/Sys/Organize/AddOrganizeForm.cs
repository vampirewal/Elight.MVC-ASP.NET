﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.Utility.ResponseModels;
using Elight.WinForm.Common;
using Elight.Utility.Other;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Elight.WinForm.Page.Sys.Organize
{
    public partial class AddOrganizeForm : UIForm
    {
        public AddOrganizeForm()
        {
            InitializeComponent();
        }

        #region 标题栏
        private void btnClose_Click(object sender, EventArgs e)
        {
            FormHelper.subForm = null;
            this.Close();
        }
        private Point mPoint;
        private void titlePanel_MouseDown(object sender, MouseEventArgs e)
        {
            mPoint = new Point(e.X, e.Y);
        }

        private void titlePanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(this.Location.X + e.X - mPoint.X, this.Location.Y + e.Y - mPoint.Y);
            }
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.FromArgb(231, 231, 231);
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackColor = Color.Transparent;

        }
        #endregion


        public OrganizePage ParentPage { get; set; }
        public string Id { get; set; }

        /// <summary>
        /// 画面加载，读取用户信息，显示在界面上
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddOrganizeForm_Load(object sender, EventArgs e)
        {
            comboType.Items.Add("公司");
            comboType.Items.Add("部门");
            comboType.Items.Add("小组");
            comboType.SelectedIndex = 1;

            if (StringHelper.IsNullOrEmpty(Id))
            {
                lblTitle.Text = "新增机构";
                txtEnCode.Enabled = true;
            }
            else
            {
                lblTitle.Text = "修改机构";
                txtEnCode.Enabled = false;
            }
            //获取部门下拉列表的值
            if (StringHelper.IsNullOrEmpty(Id))
            {
                return;
            }
            Dictionary<string, string> parm = new Dictionary<string, string>();
            parm.Add("primaryKey", Id);
            //获得用户信息
            string url = GlobalConfig.Url + "app/system/organize/getForm";
            string str = HttpUtils.DoPost(url, parm, 1000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                btnClose_Click(null, null);
                return;
            }
            SysOrganize user = null;
            try
            {
                user = str.ToObject<SysOrganize>();
            }
            catch
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                btnClose_Click(null, null);
            }
            //给文本框赋值
            txtEnCode.Text = user.EnCode;
            txtName.Text = user.FullName;
            comboType.SelectedIndex = user.Type.Value;
            txtManagerId.Text = user.ManagerId;
            txtTelePhone.Text = user.TelePhone;
            txtWeChat.Text = user.WeChat;
            txtEmail.Text = user.Email;
            txtFax.Text = user.Fax;
            txtAddress.Text = user.Address;
            txtSortCode.Value = user.SortCode.Value;
            txtRemark.Text = user.Remark;
        }


        /// <summary>
        /// 确定按钮点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (StringHelper.IsNullOrEmpty(Id))
            {
                DoAdd();
            }
            else
            {
                DoUpdate();
            }
        }

        /// <summary>
        /// 执行更新操作
        /// </summary>
        private void DoUpdate()
        {
            bool flag = ChechEmpty();
            if (!flag)
            {
                return;
            }
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("Id", Id);
            parms.Add("EnCode", txtEnCode.Text);
            parms.Add("FullName", txtName.Text);
            parms.Add("Type", Convert.ToString(comboType.SelectedIndex));
            parms.Add("ManagerId", txtManagerId.Text);
            parms.Add("TelePhone", txtTelePhone.Text);
            parms.Add("WeChat", txtWeChat.Text);
            parms.Add("Email", txtEmail.Text);
            parms.Add("Fax", txtFax.Text);
            parms.Add("Address", txtAddress.Text);
            parms.Add("SortCode", Convert.ToString(txtSortCode.Value));
            parms.Add("Remark", txtRemark.Text);
            parms.Add("ModifyUser", GlobalConfig.CurrentUser.Account);

            string url = GlobalConfig.Url + "app/system/organize/form";
            string str = HttpUtils.DoPost(url, parms, 2000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.Init();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }

        /// <summary>
        /// 数据校验
        /// </summary>
        /// <param name="checkPassword"></param>
        /// <returns></returns>
        private bool ChechEmpty()
        {
            if (StringHelper.IsNullOrEmpty(txtEnCode.Text))
            {
                this.ShowWarningDialog("编码不能为空", UIStyle.White);
                return false;
            }
            if (StringHelper.IsNullOrEmpty(txtName.Text))
            {
                this.ShowWarningDialog("名称不能为空", UIStyle.White);
                return false;
            }
            if (StringHelper.IsNullOrEmpty(comboType.SelectedItem.ToString()))
            {
                this.ShowWarningDialog("类型不能为空", UIStyle.White);
                return false;
            }
            return true;
        }


        /// <summary>
        /// 执行新增操作
        /// </summary>
        private void DoAdd()
        {
            bool flag = ChechEmpty();
            if (!flag)
                return;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms.Add("EnCode", txtEnCode.Text);
            parms.Add("FullName", txtName.Text);
            parms.Add("Type", Convert.ToString(comboType.SelectedIndex));
            parms.Add("ManagerId", txtManagerId.Text);
            parms.Add("TelePhone", txtTelePhone.Text);
            parms.Add("WeChat", txtWeChat.Text);
            parms.Add("Email", txtEmail.Text);
            parms.Add("Fax", txtFax.Text);
            parms.Add("Address", txtAddress.Text);
            parms.Add("SortCode", Convert.ToString(txtSortCode.Value));
            parms.Add("Remark", txtRemark.Text);
            parms.Add("CreateUser", GlobalConfig.CurrentUser.Account);
            string url = GlobalConfig.Url + "app/system/organize/form";
            string str = HttpUtils.DoPost(url, parms, 2000);
            if (StringHelper.IsNullOrEmpty(str))
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            try
            {
                AjaxResult result = str.ToObject<AjaxResult>();
                if (result.state != ResultType.Success)
                {
                    this.ShowWarningDialog(result.message, UIStyle.White);
                    return;
                }
                ParentPage.Init();
                btnClose_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowWarningDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
        }
    }
}
