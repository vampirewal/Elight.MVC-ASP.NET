﻿using Elight.Entity.Sys;
using Elight.Utility.Core;
using Elight.Utility.Network;
using Elight.WinForm.Common;
using Elight.WinForm.Page;
using Elight.WinForm.Page.Sys.User;
using Elight.WinForm.Page.Sys.Item;
using Elight.WinForm.Page.Sys.Organize;
using Elight.WinForm.Page.Sys.Permission;
using Elight.WinForm.Page.Sys.Role;
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Elight.WinForm.Page.Sys.Logs;
using System.Reflection;

namespace Elight.WinForm
{
    /// <summary>
    /// 主界面
    /// </summary>
    public partial class MainForm : UIAsideMainFrame
    {
        public MainForm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// 画面加载 显示菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            //设置关联
            Aside.TabControl = MainTabControl;
            //获得用户权限
            string url = GlobalConfig.Url + "app/home/getPermission";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("userId", GlobalConfig.CurrentUser.Id);
            string str = HttpUtils.DoGet(url, parameters, 2000);
            List<SysPermission> list;
            try
            {
                list = str.ToList<SysPermission>();
            }
            catch
            {
                list = null;
            }
            if (list == null)
            {
                this.ShowInfoDialog("网络或服务器异常，请稍后重试", UIStyle.White);
                return;
            }
            GlobalConfig.PermissionList = list;
            Guid guid = Guid.NewGuid();
            AddPage(new DefaultPage(), guid);
            SelectPage(guid);//显示默认页


            //取出一级菜单
            List<SysPermission> root = list.Where(it => it.ParentId == "0").ToList();
            foreach (SysPermission permission in root)
            {
                TreeNode parent = Aside.CreateNode(permission.Name, permission.SymbolIndex, 24, permission.SortCode.Value);
                List<SysPermission> childList = list.Where(it => it.ParentId == permission.Id).ToList();
                foreach (SysPermission child in childList)
                {
                    Aside.CreateChildNode(parent, AddPage(CreateUIPage(child), child.SortCode.Value));
                }
            }
            //Aside.SelectFirst();
        }


        /// <summary>
        /// 根据不同的权限，创建不同的Page
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        private MyPage CreateUIPage(SysPermission permission)
        {
            //反射创建具体的页面
            List<Type> typeList = Assembly.GetExecutingAssembly().GetTypes().Where(it => it.IsClass).ToList();
            foreach (Type type in typeList)
            {
                PageCodeAttribute attribute = type.GetCustomAttribute(typeof(PageCodeAttribute)) as PageCodeAttribute;
                if (attribute == null)
                    continue;
                if (attribute.Encode != permission.EnCode)
                    continue;
                try
                {
                    MyPage page = (MyPage)type.CreateInstance();
                    page.Text = permission.Name;
                    page.Symbol = permission.SymbolIndex;
                    page.ButtonPermissionList = GlobalConfig.PermissionList.Where(it => it.ParentId == permission.Id).ToList();
                    return page;
                }
                catch
                {
                    MyPage page = new EmptyPage();
                    page.Text = permission.Name;
                    page.Symbol = permission.SymbolIndex;
                    return page;
                }
            }
            MyPage page1 = new EmptyPage();
            page1.Text = permission.Name;
            page1.Symbol = permission.SymbolIndex;
            return page1;
        }

        /// <summary>
        /// 退出提示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.ShowAskDialog("您确定要退出吗？", UIStyle.White))
            {
                Environment.Exit(0);
                return;
            }
            e.Cancel = true;
        }


        /// <summary>
        /// 菜单选择
        /// </summary>
        /// <param name="node"></param>
        /// <param name="item"></param>
        /// <param name="pageIndex"></param>
        private void Aside_MenuItemClick(TreeNode node, NavMenuItem item, int pageIndex)
        {
            GlobalConfig.CurrentMenuText = node.Text;
        }
    }
}
