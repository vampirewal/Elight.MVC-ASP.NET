﻿using Elight.Entity.Sys;
using Elight.Logic.Base;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Elight.Utility.Operator;
using Elight.Utility.Security;
using Elight.Utility.Extension;
using Elight.Utility.Core;
using Elight.Utility.Other;

namespace Elight.Logic.Sys
{
    public class SysUserLogic : BaseLogic
    {
        /// <summary>
        /// 根据账号得到用户信息
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public SysUser GetByUserName(string account)
        {
            using (var db = GetInstance())
            {
                return db.Queryable<SysUser, SysOrganize>((A, B) => new object[] {
                    JoinType.Left,B.Id == A.DepartmentId
                }).Where((A, B) => A.Account == account).Select((A, B) => new SysUser
                {
                    Id = A.Id,
                    Account = A.Account,
                    RealName = A.RealName,
                    NickName = A.NickName,
                    Avatar = A.Avatar,
                    Gender = A.Gender,
                    Birthday = A.Birthday,
                    MobilePhone = A.MobilePhone,
                    Email = A.Email,
                    Signature = A.Signature,
                    Address = A.Address,
                    CompanyId = A.CompanyId,
                    IsEnabled = A.IsEnabled,
                    SortCode = A.SortCode,
                    DepartmentId = A.DepartmentId,
                    DeleteMark = A.DeleteMark,
                    CreateUser = A.CreateUser,
                    CreateTime = A.CreateTime,
                    ModifyUser = A.ModifyUser,
                    ModifyTime = A.ModifyTime,
                    DeptName = B.FullName
                }).First();
            }
        }

        /// <summary>
        /// 修改用户基础信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int UpdateBasicInfo(SysUser model, string account)
        {
            using (var db = GetInstance())
            {
                model.ModifyUser = account;
                model.ModifyTime = DateTime.Now;
                return db.Updateable<SysUser>(model).UpdateColumns(it => new
                {
                    it.RealName,
                    it.NickName,
                    it.Gender,
                    it.Birthday,
                    it.MobilePhone,
                    it.Avatar,
                    it.Email,
                    it.Signature,
                    it.Address,
                    it.ModifyUser,
                    it.ModifyTime
                }).ExecuteCommand();
            }
        }

        public int AppUpdateBasicInfo(SysUser model)
        {
            using (var db = GetInstance())
            {
                model.ModifyUser = model.Account;
                model.ModifyTime = DateTime.Now;
                return db.Updateable<SysUser>(model).UpdateColumns(it => new
                {
                    it.RealName,
                    it.NickName,
                    it.Gender,
                    it.Birthday,
                    it.MobilePhone,
                    it.Avatar,
                    it.Email,
                    it.Signature,
                    it.Address,
                    it.ModifyUser,
                    it.ModifyTime
                }).ExecuteCommand();
            }
        }

        public int Insert(SysUser model, string password, string account, string[] roleIds)
        {
            using (var db = GetInstance())
            {
                try
                {
                    db.BeginTran();
                    ////新增用户基本信息。
                    model.Id = UUID.StrSnowId;
                    model.DeleteMark = "0";
                    model.CreateUser = account;
                    model.CreateTime = DateTime.Now;
                    model.ModifyUser = model.CreateUser;
                    model.ModifyTime = model.CreateTime;
                    model.Avatar = "/Content/framework/images/avatar.png";
                    int row = db.Insertable<SysUser>(model).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    //删除原来的角色
                    //row = db.Deleteable<SysUserRoleRelation>().Where(it => it.UserId == model.Id).ExecuteCommand();
                    //if (row == 0)
                    //{
                    //    db..RollbackTran();
                    //    return row;
                    //}
                    //新增新的角色
                    List<SysUserRoleRelation> list = new List<SysUserRoleRelation>();
                    foreach (string roleId in roleIds)
                    {
                        SysUserRoleRelation roleRelation = new SysUserRoleRelation
                        {
                            Id = UUID.StrSnowId,
                            UserId = model.Id,
                            RoleId = roleId,
                            CreateUser = account,
                            CreateTime = DateTime.Now
                        };
                        list.Add(roleRelation);
                    }
                    row = db.Insertable<SysUserRoleRelation>(list).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    //新增用户登陆信息。
                    SysUserLogOn userLogOnEntity = new SysUserLogOn();
                    userLogOnEntity.Id = UUID.StrSnowId;
                    userLogOnEntity.UserId = model.Id;
                    userLogOnEntity.SecretKey = userLogOnEntity.Id.DESEncrypt().Substring(0, 8);
                    userLogOnEntity.Password = password.MD5Encrypt().DESEncrypt(userLogOnEntity.SecretKey).MD5Encrypt();
                    userLogOnEntity.LoginCount = 0;
                    userLogOnEntity.IsOnLine = "0";
                    row = db.Insertable<SysUserLogOn>(userLogOnEntity).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    db.CommitTran();
                    return row;
                }
                catch
                {
                    db.RollbackTran();
                    return 0;
                }
            }
        }

        public bool ContainsUser(string userAccount, params string[] userIdList)
        {
            using (var db = GetInstance())
            {
                List<string> accountList = db.Queryable<SysUser>().Where(it => userIdList.Contains(it.Id)).Select(it => it.Account).ToList();
                if (accountList.IsNullOrEmpty())
                    return false;
                if (accountList.Contains(userAccount))
                    return true;
                return false;
            }
        }
        public int AppInsert(SysUser model, string password, string[] roleIds, string opearateUser)
        {
            using (var db = GetInstance())
            {
                try
                {
                    db.BeginTran();
                    ////新增用户基本信息。
                    model.Id = UUID.StrSnowId;
                    model.DeleteMark = "0";
                    model.IsEnabled = "1";
                    model.CreateUser = opearateUser;
                    model.CreateTime = DateTime.Now;
                    model.ModifyUser = model.CreateUser;
                    model.ModifyTime = model.CreateTime;
                    model.Avatar = "/Content/framework/images/avatar.png";
                    int row = db.Insertable<SysUser>(model).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    //删除原来的角色
                    //row = db.Deleteable<SysUserRoleRelation>().Where(it => it.UserId == model.Id).ExecuteCommand();
                    //if (row == 0)
                    //{
                    //    db..RollbackTran();
                    //    return row;
                    //}
                    //新增新的角色
                    List<SysUserRoleRelation> list = new List<SysUserRoleRelation>();
                    foreach (string roleId in roleIds)
                    {
                        SysUserRoleRelation roleRelation = new SysUserRoleRelation
                        {
                            Id = UUID.StrSnowId,
                            UserId = model.Id,
                            RoleId = roleId,
                            CreateUser = opearateUser,
                            CreateTime = DateTime.Now
                        };
                        list.Add(roleRelation);
                    }
                    row = db.Insertable<SysUserRoleRelation>(list).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    //新增用户登陆信息。
                    SysUserLogOn userLogOnEntity = new SysUserLogOn();
                    userLogOnEntity.Id = UUID.StrSnowId;
                    userLogOnEntity.UserId = model.Id;
                    userLogOnEntity.SecretKey = userLogOnEntity.Id.DESEncrypt().Substring(0, 8);
                    userLogOnEntity.Password = password.DESEncrypt(userLogOnEntity.SecretKey).MD5Encrypt();
                    userLogOnEntity.LoginCount = 0;
                    userLogOnEntity.IsOnLine = "0";
                    row = db.Insertable<SysUserLogOn>(userLogOnEntity).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    db.CommitTran();
                    return row;
                }
                catch
                {
                    db.RollbackTran();
                    return 0;
                }


            }
        }


        /// <summary>
        /// 根据主键得到用户信息
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public SysUser Get(string primaryKey)
        {
            using (var db = GetInstance())
            {
                return db.Queryable<SysUser, SysOrganize>((A, B) => new object[] {
                    JoinType.Left,B.Id == A.DepartmentId
                }).Where((A, B) => A.Id == primaryKey).Select((A, B) => new SysUser
                {
                    Id = A.Id,
                    Account = A.Account,
                    RealName = A.RealName,
                    NickName = A.NickName,
                    Avatar = A.Avatar,
                    Gender = A.Gender,
                    Birthday = A.Birthday,
                    MobilePhone = A.MobilePhone,
                    Email = A.Email,
                    Signature = A.Signature,
                    Address = A.Address,
                    CompanyId = A.CompanyId,
                    IsEnabled = A.IsEnabled,
                    SortCode = A.SortCode,
                    DepartmentId = A.DepartmentId,
                    DeleteMark = A.DeleteMark,
                    CreateUser = A.CreateUser,
                    CreateTime = A.CreateTime,
                    ModifyUser = A.ModifyUser,
                    ModifyTime = A.ModifyTime,
                    DeptName = B.FullName
                }).First();
            }
        }

        /// <summary>
        /// 获得用户列表分页
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="keyWord"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<SysUser> GetList(int pageIndex, int pageSize, string keyWord, ref int totalCount)
        {
            using (var db = GetInstance())
            {
                if (keyWord.IsNullOrEmpty())
                {
                    //totalCount = db.Queryable<SysUser>().Where(it => it.DeleteMark == "0").Count();
                    return db.Queryable<SysUser, SysOrganize>((A, B) => new object[] {
                        JoinType.Left,B.Id == A.DepartmentId
                    }).Where((A, B) => A.DeleteMark == "0").OrderBy((A, B) => A.SortCode).Select((A, B) => new SysUser
                    {
                        Id = A.Id,
                        Account = A.Account,
                        RealName = A.RealName,
                        NickName = A.NickName,
                        Avatar = A.Avatar,
                        Gender = A.Gender,
                        Birthday = A.Birthday,
                        MobilePhone = A.MobilePhone,
                        Email = A.Email,
                        Signature = A.Signature,
                        Address = A.Address,
                        CompanyId = A.CompanyId,
                        IsEnabled = A.IsEnabled,
                        SortCode = A.SortCode,
                        DepartmentId = A.DepartmentId,
                        DeleteMark = A.DeleteMark,
                        CreateUser = A.CreateUser,
                        CreateTime = A.CreateTime,
                        ModifyUser = A.ModifyUser,
                        ModifyTime = A.ModifyTime,
                        DeptName = B.FullName
                    }).ToPageList(pageIndex, pageSize, ref totalCount);
                }
                //totalCount = db.Queryable<SysUser>().Where(it => it.DeleteMark == "0" && (it.Account.Contains(keyWord) || it.RealName.Contains(keyWord))).Count();
                return db.Queryable<SysUser, SysOrganize>((A, B) => new object[] {
                    JoinType.Left,B.Id == A.DepartmentId
                }).Where((A, B) => A.DeleteMark == "0" && (A.Account.Contains(keyWord) || A.RealName.Contains(keyWord))).OrderBy((A, B) => A.SortCode).Select((A, B) => new SysUser
                {
                    Id = A.Id,
                    Account = A.Account,
                    RealName = A.RealName,
                    NickName = A.NickName,
                    Avatar = A.Avatar,
                    Gender = A.Gender,
                    Birthday = A.Birthday,
                    MobilePhone = A.MobilePhone,
                    Email = A.Email,
                    Signature = A.Signature,
                    Address = A.Address,
                    CompanyId = A.CompanyId,
                    IsEnabled = A.IsEnabled,
                    SortCode = A.SortCode,
                    DepartmentId = A.DepartmentId,
                    DeleteMark = A.DeleteMark,
                    CreateUser = A.CreateUser,
                    CreateTime = A.CreateTime,
                    ModifyUser = A.ModifyUser,
                    ModifyTime = A.ModifyTime,
                    DeptName = B.FullName
                }).ToPageList(pageIndex, pageSize, ref totalCount);

            }
        }

        /// <summary>
        /// 获得用户列表分页
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="keyWord"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public List<SysUser> GetPageList(int pageIndex, int pageSize, string keyWord, ref int totalCount)
        {
            using (var db = GetInstance())
            {
                if (keyWord.IsNullOrEmpty())
                {
                    //totalCount = db.Queryable<SysUser>().Where(it => it.DeleteMark == "0").Count();
                    return db.Queryable<SysUser, SysOrganize>((A, B) => new object[] {
                        JoinType.Left,B.Id == A.DepartmentId
                    }).Where((A, B) => A.DeleteMark == "0" && B.FullName == "项目部").OrderBy((A, B) => A.SortCode).Select((A, B) => new SysUser
                    {
                        Id = A.Id,
                        Account = A.Account,
                        RealName = A.RealName,
                        NickName = A.NickName,
                        Avatar = A.Avatar,
                        Gender = A.Gender,
                        Birthday = A.Birthday,
                        MobilePhone = A.MobilePhone,
                        Email = A.Email,
                        Signature = A.Signature,
                        Address = A.Address,
                        CompanyId = A.CompanyId,
                        IsEnabled = A.IsEnabled,
                        SortCode = A.SortCode,
                        DepartmentId = A.DepartmentId,
                        DeleteMark = A.DeleteMark,
                        CreateUser = A.CreateUser,
                        CreateTime = A.CreateTime,
                        ModifyUser = A.ModifyUser,
                        ModifyTime = A.ModifyTime,
                        DeptName = B.FullName
                    }).ToPageList(pageIndex, pageSize, ref totalCount);
                }
                //totalCount = db.Queryable<SysUser>().Where(it => it.DeleteMark == "0" && (it.Account.Contains(keyWord) || it.RealName.Contains(keyWord))).Count();
                return db.Queryable<SysUser, SysOrganize>((A, B) => new object[] {
                    JoinType.Left,B.Id == A.DepartmentId
                }).Where((A, B) => A.DeleteMark == "0" && (A.Account.Contains(keyWord) || A.RealName.Contains(keyWord))).OrderBy((A, B) => A.SortCode).Select((A, B) => new SysUser
                {
                    Id = A.Id,
                    Account = A.Account,
                    RealName = A.RealName,
                    NickName = A.NickName,
                    Avatar = A.Avatar,
                    Gender = A.Gender,
                    Birthday = A.Birthday,
                    MobilePhone = A.MobilePhone,
                    Email = A.Email,
                    Signature = A.Signature,
                    Address = A.Address,
                    CompanyId = A.CompanyId,
                    IsEnabled = A.IsEnabled,
                    SortCode = A.SortCode,
                    DepartmentId = A.DepartmentId,
                    DeleteMark = A.DeleteMark,
                    CreateUser = A.CreateUser,
                    CreateTime = A.CreateTime,
                    ModifyUser = A.ModifyUser,
                    ModifyTime = A.ModifyTime,
                    DeptName = B.FullName
                }).ToPageList(pageIndex, pageSize, ref totalCount);

            }
        }

        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="primaryKeys"></param>
        /// <returns></returns>
        public int Delete(List<string> primaryKeys)
        {
            using (var db = GetInstance())
            {
                try
                {
                    db.BeginTran();
                    foreach (string primaryKey in primaryKeys)
                    {
                        db.Deleteable<SysUser>().Where(it => it.Id == primaryKey).ExecuteCommand();
                    }
                    db.CommitTran();
                    return 1;
                }
                catch (Exception ex)
                {
                    db.RollbackTran();
                    return 0;
                }
            }
        }

        /// <summary>
        /// 新增用户基础信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(SysUser model, string account)
        {
            using (var db = GetInstance())
            {
                model.Id = UUID.StrSnowId;
                model.DeleteMark = "0";
                model.CreateUser = account;
                model.CreateTime = DateTime.Now;
                model.ModifyUser = model.CreateUser;
                model.ModifyTime = model.CreateTime;
                model.Avatar = "/Content/framework/images/avatar.png";
                return db.Insertable<SysUser>(model).ExecuteCommand();
            }
        }
        /// <summary>
        /// 更新用户基础信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Update(SysUser model, string account)
        {
            using (var db = GetInstance())
            {
                model.ModifyUser = account;
                model.ModifyTime = DateTime.Now;

                return db.Updateable<SysUser>(model).UpdateColumns(it => new
                {
                    it.NickName,
                    it.RealName,
                    it.Birthday,
                    it.Gender,
                    it.Email,
                    it.DepartmentId,
                    it.RoleId,
                    it.MobilePhone,
                    it.Address,
                    it.Signature,
                    it.SortCode,
                    it.IsEnabled,
                    it.ModifyUser,
                    it.ModifyTime
                }).ExecuteCommand();
            }
        }



        public int AppUpdateAndSetRole(SysUser model, string[] roleIds, string opereateUser)
        {
            using (var db = GetInstance())
            {
                try
                {
                    db.BeginTran();
                    model.ModifyUser = opereateUser;
                    model.ModifyTime = DateTime.Now;
                    int row = db.Updateable<SysUser>(model).UpdateColumns(it => new
                    {
                        it.NickName,
                        it.RealName,
                        it.Birthday,
                        it.Gender,
                        it.Email,
                        it.DepartmentId,
                        it.RoleId,
                        it.MobilePhone,
                        it.Address,
                        it.Signature,
                        it.SortCode,
                        it.ModifyUser,
                        it.ModifyTime
                    }).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    //删除原来的角色
                    db.Deleteable<SysUserRoleRelation>().Where(it => it.UserId == model.Id).ExecuteCommand();
                    //新增新的角色
                    List<SysUserRoleRelation> list = new List<SysUserRoleRelation>();
                    foreach (string roleId in roleIds)
                    {
                        SysUserRoleRelation roleRelation = new SysUserRoleRelation
                        {
                            Id = UUID.StrSnowId,
                            UserId = model.Id,
                            RoleId = roleId,
                            CreateUser = opereateUser,
                            CreateTime = DateTime.Now
                        };
                        list.Add(roleRelation);
                    }
                    row = db.Insertable<SysUserRoleRelation>(list).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    db.CommitTran();
                    return row;
                }
                catch
                {
                    db.RollbackTran();
                    return 0;
                }
            }
        }


        public int UpdateAndSetRole(SysUser model, string account, string[] roleIds)
        {
            using (var db = GetInstance())
            {
                try
                {
                    db.BeginTran();
                    model.ModifyUser = account;
                    model.ModifyTime = DateTime.Now;
                    int row = db.Updateable<SysUser>(model).UpdateColumns(it => new
                    {
                        it.NickName,
                        it.RealName,
                        it.Birthday,
                        it.Gender,
                        it.Email,
                        it.DepartmentId,
                        it.RoleId,
                        it.MobilePhone,
                        it.Address,
                        it.Signature,
                        it.SortCode,
                        it.IsEnabled,
                        it.ModifyUser,
                        it.ModifyTime
                    }).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    //删除原来的角色
                    db.Deleteable<SysUserRoleRelation>().Where(it => it.UserId == model.Id).ExecuteCommand();
                    //新增新的角色
                    List<SysUserRoleRelation> list = new List<SysUserRoleRelation>();
                    foreach (string roleId in roleIds)
                    {
                        SysUserRoleRelation roleRelation = new SysUserRoleRelation
                        {
                            Id = UUID.StrSnowId,
                            UserId = model.Id,
                            RoleId = roleId,
                            CreateUser = account,
                            CreateTime = DateTime.Now
                        };
                        list.Add(roleRelation);
                    }
                    row = db.Insertable<SysUserRoleRelation>(list).ExecuteCommand();
                    if (row == 0)
                    {
                        db.RollbackTran();
                        return row;
                    }
                    db.CommitTran();
                    return row;
                }
                catch
                {
                    db.RollbackTran();
                    return 0;
                }
            }
        }
    }
}
