﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Elight.Utility.Extension;
using UEditorNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.OpenApi.Models;
using Elight.WebUI.Filters;
using Elight.Entity.GlobalData;
using SoapCore;
using Elight.Service.WebService;

namespace Elight.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(GlobalValue.Config.SessionTimeout);
            });
            //使用缓存
            services.AddMemoryCache();
            services.AddMvc();
            services.AddControllers();
            services.AddUEditorService("Configs/ueditor.json");
            //services.AddRazorPages();
            //注册Swagger生成器，定义一个和多个Swagger 文档
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    // {ApiName} 定义成全局变量，方便修改
                    Version = "v1",
                    Title = "My API"
                });
                //在接口类、方法标记属性 [HiddenApi]，可以阻止【Swagger文档】生成 
                c.DocumentFilter<HiddenApiFilter>();
                c.OrderActionsBy(o => o.RelativePath);
                //var basePath = System.AppDomain.CurrentDomain.BaseDirectory;//获取应用程序所在目录（绝对，不受工作目录影响，建议采用此方法获取路径）
                //var xmlPath = Path.Combine(basePath, "Elight.WebUI.Core.xml");
                //if (File.Exists(xmlPath))//避免没有该文件时报错
                //    c.IncludeXmlComments(xmlPath);
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseDeveloperExceptionPage();
            app.UseExceptionHandler("/error");
            app.UseStatusCodePagesWithReExecute("/error/notFound/{0}");
            app.UseHsts();

            MyHttpContext.ServiceProvider = app.ApplicationServices;
            //启用Session
            app.UseSession();

            //配置WebService
            app.UseSoapEndpoint<WebServiceContract>("/WebService.asmx", new SoapEncoderOptions());

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API v1");
            });


            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
