﻿using System.Collections.Generic;
using Elight.WebUI.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using Elight.Utility.Core;
using Elight.Utility.Operator;
using Elight.WebUI.Controllers;

namespace Elight.WebUI.Areas.Sys.Controllers
{
    [HiddenApi]
    [Area("Sys")]
    public class ItemController : BaseController
    {
        private SysItemLogic itemLogic;
        private SysItemsDetailLogic itemsDetailLogic;

        public ItemController()
        {
            itemLogic = new SysItemLogic();
            itemsDetailLogic = new SysItemsDetailLogic();
        }


        [Route("system/item/index")]
        [HttpGet, AuthorizeChecked]
        public ActionResult Index()
        {
            return View();
        }

        [Route("system/item/index")]
        [HttpPost, AuthorizeChecked]
        public ActionResult Index(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = itemLogic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysItem>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }


        [Route("system/item/form")]
        [HttpGet, LoginChecked]
        public ActionResult Form()
        {
            return View();
        }


        [Route("system/item/form")]
        [HttpPost, LoginChecked]
        public ActionResult Form(SysItem model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = itemLogic.Insert(model, OperatorProvider.Instance.Current.Account);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = itemLogic.Update(model, OperatorProvider.Instance.Current.Account);
                return row > 0 ? Success() : Error();
            }
        }

        [Route("system/item/getForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysItem entity = itemLogic.Get(primaryKey);
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }





        [Route("system/item/delete")]
        [HttpPost, LoginChecked]
        public ActionResult Delete(string primaryKey)
        {
            int count = itemLogic.GetChildCount(primaryKey);
            if (count == 0)
            {
                //删除字典。
                int row = itemLogic.Delete(primaryKey);
                //删除字典选项。
                itemsDetailLogic.Delete(primaryKey);
                return row > 0 ? Success() : Error();
            }
            return Warning(string.Format("操作失败，请先删除该项的{0}个子级字典。", count));
        }


        [Route("system/item/detail")]
        [HttpGet, LoginChecked]
        public ActionResult Detail()
        {
            return View();
        }

        [Route("system/item/getListTree")]
        [HttpPost, LoginChecked]
        public ActionResult GetListTree()
        {
            var listAllItems = itemLogic.GetList();
            List<ZTreeNode> result = new List<ZTreeNode>();
            foreach (var item in listAllItems)
            {
                ZTreeNode model = new ZTreeNode();
                model.id = item.Id;
                model.pId = item.ParentId;
                model.name = item.Name;
                model.open = true;
                result.Add(model);
            }
            return Content(result.ToJson());
        }
        [Route("system/item/getListSelectTree")]
        [HttpPost, LoginChecked]
        public ActionResult GetListSelectTree()
        {
            var data = itemLogic.GetList();
            var treeList = new List<TreeSelect>();
            foreach (var item in data)
            {
                TreeSelect model = new TreeSelect();
                model.id = item.Id;
                model.text = item.Name;
                model.parentId = item.ParentId;
                treeList.Add(model);
            }
            return Content(treeList.ToTreeSelectJson());
        }

        [HttpPost, Route("app/system/item/index")]
        public ActionResult AppIndex(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = itemLogic.GetAppList(pageIndex, pageSize, ref totalCount);
            var result = new LayPadding<SysItem>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }
        [HttpPost, Route("app/system/item/form")]
        public ActionResult AppForm(SysItem model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = itemLogic.AppInsert(model, model.CreateUser);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = itemLogic.AppUpdate(model, model.ModifyUser);
                return row > 0 ? Success() : Error();
            }
        }
        [HttpPost, Route("app/system/item/getForm")]
        public ActionResult AppGetForm(string primaryKey)
        {
            SysItem entity = itemLogic.Get(primaryKey);
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }
        [HttpPost, Route("app/system/item/delete")]
        public ActionResult AppDelete(string primaryKey)
        {
            int count = itemLogic.GetChildCount(primaryKey);
            if (count == 0)
            {
                //删除字典。
                int row = itemLogic.Delete(primaryKey);
                //删除字典选项。
                itemsDetailLogic.Delete(primaryKey);
                return row > 0 ? Success() : Error();
            }
            return Warning($"操作失败，请先删除该项的{count}个子级字典。");
        }

        [HttpGet, Route("app/system/item/getListTree")]
        public ActionResult AppGetListTree()
        {
            var listAllItems = itemLogic.GetList();
            List<ZTreeNode> result = new List<ZTreeNode>();
            foreach (var item in listAllItems)
            {
                ZTreeNode model = new ZTreeNode();
                model.id = item.Id;
                model.pId = item.ParentId;
                model.name = item.Name;
                model.open = true;
                result.Add(model);
            }
            return Content(result.ToJson());
        }
    }
}
