﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Elight.WebUI.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Core;
using Elight.Utility.Operator;
using Elight.WebUI.Controllers;

namespace MES.WebUI.Areas.Sys.Controllers
{
    [HiddenApi]
    [Area("Sys")]
    public class PermissionController : BaseController
    {
        private SysPermissionLogic logic;

        /// <summary>
        /// 构造方法
        /// </summary>
        public PermissionController()
        {
            logic = new SysPermissionLogic();
        }


        /// <summary>
        /// 主界面
        /// </summary>
        /// <returns></returns> 
        [HttpGet, Route("system/permission/index"), AuthorizeChecked]
        public ActionResult Index()
        {
            return View();
        }



        /// <summary>
        /// 主界面条件检索数据
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="keyWord"></param>
        /// <returns></returns>
        [HttpPost, Route("system/permission/index"), AuthorizeChecked]
        public ActionResult Index(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = logic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysPermission>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count,
            };
            return Content(result.ToJson());
        }




        [Route("system/permission/form")]
        [HttpGet, AuthorizeChecked]
        public ActionResult Form()
        {
            return View();
        }



        [HttpPost, Route("system/permission/form"), AuthorizeChecked]
        public ActionResult Form(SysPermission model)
        {
            //当前类型是啥
            if (model.Type == 2)
            {
                model.ParentId = "0";
            }
            else if (model.Type == 0)
            {
                SysPermission permission = logic.Get(model.ParentId);
                if (permission.Type != 2)
                    return Error("当前类型的父级必须为主菜单");
            }
            else
            {
                SysPermission permission = logic.Get(model.ParentId);
                if (permission.Type != 0)
                    return Error("当前类型的父级必须为子菜单");
            }
            if (model.Id.IsNullOrEmpty())
            {
                int row = logic.Insert(model, OperatorProvider.Instance.Current.Account);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = logic.Update(model, OperatorProvider.Instance.Current.Account);
                return row > 0 ? Success() : Error();
            }
        }


        [HttpGet, Route("system/permission/detail"), AuthorizeChecked]
        public ActionResult Detail()
        {
            return View();
        }



        [HttpPost, Route("system/permission/delete"), AuthorizeChecked]
        public ActionResult Delete(string primaryKey)
        {
            long count = logic.GetChildCount(primaryKey);
            if (count == 0)
            {
                int row = logic.Delete(primaryKey.SplitToList().ToArray());
                return row > 0 ? Success() : Error();
            }
            return Error(string.Format("操作失败，请先删除该项的{0}个子级权限。", count));
        }



        [Route("system/permission/getForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysPermission entity = logic.Get(primaryKey);
            entity.IsEdit = entity.IsEdit == "1" ? "true" : "false";
            entity.IsEnable = entity.IsEnable == "1" ? "true" : "false";
            entity.IsPublic = entity.IsPublic == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }




        [Route("system/permission/getParent")]
        [HttpPost]
        public ActionResult GetParent()
        {
            var data = logic.GetList();
            var treeList = new List<TreeSelect>();
            foreach (SysPermission item in data)
            {
                TreeSelect model = new TreeSelect();
                model.id = item.Id;
                model.text = item.Name;
                model.parentId = item.ParentId;
                treeList.Add(model);
            }
            return Content(treeList.ToTreeSelectJson());
        }

        [Route("system/permission/icon")]
        [HttpGet]
        public ActionResult Icon()
        {
            return View();
        }


        [HttpPost, Route("app/system/permission/index")]
        public ActionResult AppIndex(int pageIndex, int pageSize, string keyWord)
        {
            int totalCount = 0;
            var pageData = logic.GetList(pageIndex, pageSize, keyWord, ref totalCount);
            var result = new LayPadding<SysPermission>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count,
            };
            return Content(result.ToJson());
        }
        [Route("app/system/permission/form")]
        [HttpPost]
        public ActionResult AppForm(SysPermission model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = logic.AppInsert(model, model.CreateUser);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = logic.AppUpdate(model, model.ModifyUser);
                return row > 0 ? Success() : Error();
            }
        }
        [Route("app/system/permission/delete")]
        [HttpPost]
        public ActionResult AppDelete(string primaryKey)
        {
            long count = logic.GetChildCount(primaryKey);
            if (count == 0)
            {
                int row = logic.Delete(primaryKey.SplitToList().ToArray());
                return row > 0 ? Success() : Error();
            }
            return Error(string.Format("操作失败，请先删除该项的{0}个子级权限。", count));
        }


        [Route("app/system/permission/getForm")]
        [HttpPost]
        public ActionResult AppGetForm(string primaryKey)
        {
            SysPermission entity = logic.Get(primaryKey);
            entity.IsEdit = entity.IsEdit == "1" ? "true" : "false";
            entity.IsEnable = entity.IsEnable == "1" ? "true" : "false";
            entity.IsPublic = entity.IsPublic == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }
        [Route("app/system/permission/getParent")]
        [HttpGet]
        public ActionResult AppGetParent()
        {
            var data = logic.GetList();
            var treeList = new List<TreeSelect>();
            foreach (SysPermission item in data)
            {
                TreeSelect model = new TreeSelect();
                model.id = item.Id;
                model.text = item.Name;
                model.parentId = item.ParentId;
                treeList.Add(model);
            }
            return Content(treeList.ToTreeSelectJson());
        }

    }

}
