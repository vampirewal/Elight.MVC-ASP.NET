﻿using Microsoft.AspNetCore.Mvc;
using Elight.WebUI.Filters;
using Elight.Logic.Sys;
using Elight.Entity.Sys;
using Elight.Utility.ResponseModels;
using Elight.Utility.Core;
using Elight.Utility.Operator;
using Elight.WebUI.Controllers;

namespace MES.WebUI.Areas.Sys.Controllers
{
    [HiddenApi]
    [Area("Sys")]
    public class ItemsDetailController : BaseController
    {
        private SysItemsDetailLogic itemDetaillogic;

        public ItemsDetailController()
        {
            itemDetaillogic = new SysItemsDetailLogic();
        }


        [Route("system/itemsDetail/index")]
        [HttpGet, AuthorizeChecked]
        public ActionResult Index()
        {
            return View();
        }

        [Route("system/itemsDetail/index")]
        [HttpPost, AuthorizeChecked]
        public ActionResult Index(int pageIndex, int pageSize, string itemId, string keyWord)
        {
            int totalCount = 0;
            var pageData = itemDetaillogic.GetList(pageIndex, pageSize, itemId, keyWord, ref totalCount);
            var result = new LayPadding<SysItemDetail>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }




        [Route("system/itemsDetail/form")]
        [HttpGet, AuthorizeChecked]
        public ActionResult Form()
        {
            return View();
        }


        [Route("system/itemsDetail/form")]
        [HttpPost, AuthorizeChecked]
        public ActionResult Form(SysItemDetail model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = itemDetaillogic.Insert(model, OperatorProvider.Instance.Current.Account);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = itemDetaillogic.Update(model, OperatorProvider.Instance.Current.Account);
                return row > 0 ? Success() : Error();
            }
        }

        [Route("system/itemsDetail/detail")]
        [HttpGet, AuthorizeChecked]
        public ActionResult Detail()
        {
            return View();
        }
        [Route("system/itemsDetail/delete")]
        [HttpPost, AuthorizeChecked]
        public ActionResult Delete(string primaryKey)
        {
            int row = itemDetaillogic.Delete(primaryKey);
            return row > 0 ? Success() : Error();
        }

        [Route("system/itemsDetail/getForm")]
        [HttpPost, LoginChecked]
        public ActionResult GetForm(string primaryKey)
        {
            SysItemDetail entity = itemDetaillogic.Get(primaryKey);
            entity.IsDefault = entity.IsDefault == "1" ? "true" : "false";
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }


        [HttpPost, Route("app/system/itemsDetail/index")]
        public ActionResult AppIndex(int pageIndex, int pageSize, string itemId, string keyWord)
        {
            int totalCount = 0;
            var pageData = itemDetaillogic.GetList(pageIndex, pageSize, itemId, "", ref totalCount);
            var result = new LayPadding<SysItemDetail>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = totalCount//pageData.Count
            };
            return Content(result.ToJson());
        }
        [HttpPost, Route("app/system/itemsDetail/form")]
        public ActionResult AppForm(SysItemDetail model)
        {
            if (model.Id.IsNullOrEmpty())
            {
                int row = itemDetaillogic.AppInsert(model, model.CreateUser);
                return row > 0 ? Success() : Error();
            }
            else
            {
                int row = itemDetaillogic.AppUpdate(model, model.ModifyUser);
                return row > 0 ? Success() : Error();
            }
        }
        [HttpPost, Route("app/system/itemsDetail/delete")]
        public ActionResult AppDelete(string primaryKey)
        {
            int row = itemDetaillogic.Delete(primaryKey);
            return row > 0 ? Success() : Error();
        }
        [HttpPost, Route("app/system/itemsDetail/getForm")]
        public ActionResult AppGetForm(string primaryKey)
        {
            SysItemDetail entity = itemDetaillogic.Get(primaryKey);
            entity.IsDefault = entity.IsDefault == "1" ? "true" : "false";
            entity.IsEnabled = entity.IsEnabled == "1" ? "true" : "false";
            return Content(entity.ToJson());
        }
    }
}
