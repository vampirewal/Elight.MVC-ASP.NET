﻿using Elight.Entity.Base;
using Elight.Entity.GlobalData;
using Elight.Entity.Sys;
using Elight.Logic.Base;
using Elight.Logic.Sys;
using Elight.Utility.Core;
using Elight.Utility.Other;
using Elight.Utility.ResponseModels;
using Elight.WebUI.Controllers;
using Elight.WebUI.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Elight.WebUI.Areas.Base.Controllers
{
    [HiddenApi]
    [Area("Base")]
    public class CodeGeneratorController : BaseController
    {
        private SysPermissionLogic permissionLogic;
        private BaseLogic baseLogic;
        public CodeGeneratorController()
        {
            permissionLogic = new SysPermissionLogic();
            baseLogic = new BaseLogic();
        }


        [HttpGet, Route("base/codeGenerator/index"), AuthorizeChecked]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet, Route("base/codeGenerator/getTables")]
        public ActionResult GetTables()
        {
            List<string> list = baseLogic.GetTableList();
            var treeList = new List<TreeSelect>();
            foreach (string item in list)
            {
                TreeSelect model = new TreeSelect();
                model.id = item;
                model.text = item;
                model.parentId = "0";
                treeList.Add(model);
            }
            return Content(treeList.ToTreeSelectJson());
        }

        /// <summary>
        /// 获得表字段信息
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        [HttpGet, Route("base/codeGenerator/getTableColumnInfo")]
        public ActionResult GetTableColumnInfo(string tableName)
        {
            DataTable dt = baseLogic.GetTableColumnInfo(tableName);
            if (dt == null)
                return null;
            string className = GetClassNameByTableName(tableName);
            foreach (DataRow row in dt.Rows)
            {
                string columnName = row["ColumnName"].ToString();
                string dbTypeName = row["TypeName"].ToString();
                string propertyName = GetPropertyNameByColumnName(columnName);
                string csType = GetCSharpTypeByDbTypeName(dbTypeName);
                row["ClassName"] = className;
                row["PropertyName"] = propertyName;
                row["CSType"] = csType;
                if (row["Other"] == DBNull.Value)
                    row["Other"] = "";
            }

            List<CodeGenerator> pageData = dt.ToJson().ToList<CodeGenerator>();
            var result = new LayPadding<CodeGenerator>()
            {
                result = true,
                msg = "success",
                list = pageData,
                count = pageData.Count
            };
            return Content(result.ToJson());
        }

        [HttpPost, Route("base/codeGenerator/generator")]
        public ActionResult Generator(string TableName, string areas, string ParentId, string Icon, int SymbolIndex, List<CodeGenerator> forms)
        {
            if (TableName.IsNullOrEmpty() || areas.IsNullOrEmpty() || ParentId.IsNullOrEmpty() || Icon.IsNullOrEmpty())
                return Error("请填入所有参数");
            if (forms.Count == 0)
                return Error("请填写表信息");
            CodeGenerator primaryKeyCode = forms.Where(it => it.IsPrimaryKey == "1").FirstOrDefault();
            if (primaryKeyCode == null)
            {
                return Error("该表没有主键信息");
            }
            foreach (CodeGenerator code in forms)
            {
                if (code.Other.IsNullOrEmpty())
                    code.Other = code.PropertyName;
            }

            string entity = GenerateEntity(areas, TableName, forms);
            string logic = GenerateLogic(areas, forms, primaryKeyCode.PropertyName);
            string controller = GenerateController(areas, forms, primaryKeyCode.PropertyName);
            string indexView = GenerateViewIndex(areas, forms, primaryKeyCode.PropertyName);
            string formView = GenerateViewForm(areas, forms, primaryKeyCode.PropertyName);
            string detailView = GenerateViewDetail(areas, forms, primaryKeyCode.PropertyName);
            //主的
            string className = forms[0].ClassName;
            //根据父节点，得到最大的子节点序号
            int orderCode = permissionLogic.GetMaxChildMenuOrderCode(ParentId);
            List<SysPermission> permissionList = new List<SysPermission>();


            string codeDir = GlobalValue.Config.CodeDir;
            //写入Entity
            string entityDir = codeDir + "/Elight.Entity/" + areas;
            if (!System.IO.Directory.Exists(entityDir))
                System.IO.Directory.CreateDirectory(entityDir);
            string entityFileName = entityDir + "/" + className + ".cs";
            System.IO.File.WriteAllText(entityFileName, entity, Encoding.UTF8);
            //写入Logic
            string logicDir = codeDir + "/Elight.Logic/" + areas;
            if (!System.IO.Directory.Exists(logicDir))
                System.IO.Directory.CreateDirectory(logicDir);
            string logicFileName = logicDir + "/" + className + "Logic.cs";
            System.IO.File.WriteAllText(logicFileName, logic, Encoding.UTF8);
            //写入Controller
            string webDir = codeDir + "/Elight.WebUI/Areas/" + areas;
            if (!System.IO.Directory.Exists(webDir))
                System.IO.Directory.CreateDirectory(webDir);
            string controllerDir = webDir + "/Controllers";
            if (!System.IO.Directory.Exists(controllerDir))
                System.IO.Directory.CreateDirectory(controllerDir);
            string controllerFileName = controllerDir + "/" + className + "Controller.cs";
            System.IO.File.WriteAllText(controllerFileName, controller, Encoding.UTF8);
            string viewDir = webDir + "/Views";
            if (!System.IO.Directory.Exists(viewDir))
                System.IO.Directory.CreateDirectory(viewDir);
            viewDir = viewDir + "/" + className;
            if (!System.IO.Directory.Exists(viewDir))
                System.IO.Directory.CreateDirectory(viewDir);
            string indexViewFileName = viewDir + "/Index.cshtml";
            string formViewFileName = viewDir + "/Form.cshtml";
            string detailViewFileName = viewDir + "/Detail.cshtml";
            System.IO.File.WriteAllText(indexViewFileName, indexView, Encoding.UTF8);
            System.IO.File.WriteAllText(formViewFileName, formView, Encoding.UTF8);
            System.IO.File.WriteAllText(detailViewFileName, detailView, Encoding.UTF8);


            string id = UUID.StrSnowId;
            //主
            permissionList.Add(new SysPermission
            {
                Id = id,
                ParentId = ParentId,
                Layer = 1,
                EnCode = $"{className}-manage",
                Name = $"{className}管理",
                JsEvent = null,
                Icon = Icon,
                SymbolIndex = SymbolIndex,
                Url = $"/{areas}/{className}/index",
                Remark = "",
                Type = 0,
                SortCode = orderCode,
                IsPublic = "1",
                IsEnable = "1",
                IsEdit = "1",
                DeleteMark = "0",
                CreateUser = "admin",
                CreateTime = DateTime.Now,
                ModifyUser = "admin",
                ModifyTime = DateTime.Now
            });
            //新增
            permissionList.Add(new SysPermission
            {
                Id = UUID.StrSnowId,
                ParentId = id,
                Layer = 1,
                EnCode = $"{className}-add",
                Name = $"新增{className}",
                JsEvent = "btn_add()",
                Icon = "fa fa-plus-square-o",
                SymbolIndex = 61846,
                Url = $"/{areas}/{className}/form",
                Remark = "",
                Type = 1,
                SortCode = orderCode + 1,
                IsPublic = "1",
                IsEnable = "1",
                IsEdit = "1",
                DeleteMark = "0",
                CreateUser = "admin",
                CreateTime = DateTime.Now,
                ModifyUser = "admin",
                ModifyTime = DateTime.Now
            });
            //修改
            permissionList.Add(new SysPermission
            {
                Id = UUID.StrSnowId,
                ParentId = id,
                Layer = 1,
                EnCode = $"{className}-modify",
                Name = $"修改{className}",
                JsEvent = "btn_edit()",
                Icon = "fa fa-pencil-square-o",
                SymbolIndex = 61508,
                Url = $"/{areas}/{className}/form",
                Remark = "",
                Type = 1,
                SortCode = orderCode + 2,
                IsPublic = "1",
                IsEnable = "1",
                IsEdit = "1",
                DeleteMark = "0",
                CreateUser = "admin",
                CreateTime = DateTime.Now,
                ModifyUser = "admin",
                ModifyTime = DateTime.Now
            });
            //删除
            permissionList.Add(new SysPermission
            {
                Id = UUID.StrSnowId,
                ParentId = id,
                Layer = 1,
                EnCode = $"{className}-delete",
                Name = $"删除{className}",
                JsEvent = "btn_delete()",
                Icon = "fa fa-trash-o",
                SymbolIndex = 61460,
                Url = $"/{areas}/{className}/delete",
                Remark = "",
                Type = 1,
                SortCode = orderCode + 3,
                IsPublic = "1",
                IsEnable = "1",
                IsEdit = "1",
                DeleteMark = "0",
                CreateUser = "admin",
                CreateTime = DateTime.Now,
                ModifyUser = "admin",
                ModifyTime = DateTime.Now
            });
            //查看
            permissionList.Add(new SysPermission
            {
                Id = UUID.StrSnowId,
                ParentId = id,
                Layer = 1,
                EnCode = $"{className}-detail",
                Name = $"查看{className}",
                JsEvent = "btn_detail()",
                Icon = "fa fa-eye",
                SymbolIndex = 61550,
                Url = $"/{areas}/{className}/detail",
                Remark = "",
                Type = 1,
                SortCode = orderCode + 4,
                IsPublic = "1",
                IsEnable = "1",
                IsEdit = "1",
                DeleteMark = "0",
                CreateUser = "admin",
                CreateTime = DateTime.Now,
                ModifyUser = "admin",
                ModifyTime = DateTime.Now
            });
            permissionLogic.InsertList(permissionList);
            return Success();
        }

        private string GenerateViewDetail(string area, List<CodeGenerator> forms, string primaryKeyPropertyName)
        {
            string className = forms[0].ClassName;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("@{");
            sb.AppendLine("    Layout = null; ");
            sb.AppendLine("}");
            sb.AppendLine("");
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("    <title></title>");
            sb.AppendLine("    <meta charset=\"utf-8\">");
            sb.AppendLine("    <link rel=\"shortcut icon\" type=\"image/ico\" href=\"~/favicon.ico\" />");
            sb.AppendLine("    <link rel=\"bookmark\" type=\"image/ico\" href=\"~/favicon.ico\" />");
            sb.AppendLine("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">");
            sb.AppendLine("");
            sb.AppendLine("    <link href=\"~/Content/layui/css/layui.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/select2/css/select2.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/framework/css/console.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/framework/css/animate.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />");
            sb.AppendLine("");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("        <script src=\"/Content/jquery/jquery-1.9.1.min.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <!--[if gte IE 9]><!-->");
            sb.AppendLine("    <script src=\"~/Content/jquery/jquery.min.js\"></script>");
            sb.AppendLine("    <!--<![endif]-->");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("    <script src=\"https://cdn.staticfile.org/html5shiv/r29/html5.min.js\"></script>");
            sb.AppendLine("    <script src=\"https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <script src=\"~/Content/layui/layui.js\"></script>");
            sb.AppendLine("    <script src=\"~/Content/select2/js/select2.min.js\"></script>");
            sb.AppendLine("    <script src=\"~/Content/framework/js/global.js\"></script>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("    <form id=\"form\" class=\"layui-form\" style=\"margin-top: 25px\">");

            //重组数据，两两一组
            List<List<CodeGenerator>> listList = new List<List<CodeGenerator>>();
            List<CodeGenerator> list = forms.Where(it => it.IsPrimaryKey != "1").ToList();
            int count = list.Count;
            //可以分成多少组
            int group = 0;
            if (count % 2 == 0)
            {
                group = count / 2;
            }
            else
            {
                group = count / 2 + 1;
            }
            for (int i = 0; i < group; i++)
            {
                List<CodeGenerator> codeList = new List<CodeGenerator>();
                codeList.Add(list[i * 2]);
                if ((i * 2 + 1) < list.Count)
                {
                    codeList.Add(list[i * 2 + 1]);
                }
                listList.Add(codeList);
            }

            bool first = true;
            foreach (List<CodeGenerator> codeList in listList)
            {
                sb.AppendLine("        <div class=\"layui-form-item\">");
                foreach (CodeGenerator code in codeList)
                {
                    sb.AppendLine("            <div class=\"layui-inline\">");
                    sb.AppendLine($"                <label class=\"layui-form-label label-required\">{code.Other}</label>");
                    sb.AppendLine("                <div class=\"layui-input-inline\">");
                    if (first)
                    {
                        sb.AppendLine($"                    <input type=\"hidden\" name=\"{primaryKeyPropertyName}\" disabled/>");
                        first = false;
                    }
                    sb.AppendLine($"                    <input type=\"text\" name=\"{code.PropertyName}\" id=\"{code.PropertyName}\" lay-verify=\"required\" placeholder=\"请输入{code.Other}\" autocomplete=\"off\" class=\"layui-input\" disabled>");
                    sb.AppendLine("                </div>");
                    sb.AppendLine("            </div>");
                };
                sb.AppendLine("        </div>");
            }
            sb.AppendLine("    </form>");
            sb.AppendLine("    <script>");
            sb.AppendLine("        layui.use(['element', 'form'], function () {");
            sb.AppendLine("            var form = layui.form;");
            sb.AppendLine("            var element = layui.element; ");
            sb.AppendLine("            var primaryKey = $.getQueryString(\"primaryKey\");");
            sb.AppendLine("            if (primaryKey) {");
            sb.AppendLine("                $.ajax({");
            sb.AppendLine($"                    url: \"/{area}/{className}/getForm\",");
            sb.AppendLine("                    data: { primaryKey: primaryKey },");
            sb.AppendLine("                    type: \"post\",");
            sb.AppendLine("                    dataType: \"json\",");
            sb.AppendLine("                    async: false,");
            sb.AppendLine("                    success: function (data) {");
            sb.AppendLine("                        $(\"#form\").formSerialize(data);");
            sb.AppendLine("                    }");
            sb.AppendLine("                });");
            sb.AppendLine("            }");
            sb.AppendLine("");
            sb.AppendLine("            form.render();");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("    </script>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");
            return sb.ToString();
        }

        private string GenerateViewForm(string area, List<CodeGenerator> forms, string primaryKeyPropertyName)
        {
            string className = forms[0].ClassName;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("@{");
            sb.AppendLine("    Layout = null;");
            sb.AppendLine("}");
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("    <title></title>");
            sb.AppendLine("    <meta charset=\"utf-8\">");
            sb.AppendLine("    <link rel=\"shortcut icon\" type=\"image/ico\" href=\"~/favicon.ico\" />");
            sb.AppendLine("    <link rel=\"bookmark\" type=\"image/ico\" href=\"~/favicon.ico\" />");
            sb.AppendLine("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">");
            sb.AppendLine("");
            sb.AppendLine("    <link href=\"~/Content/layui/css/layui.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/select2/css/select2.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/framework/css/console.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/framework/css/animate.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />");
            sb.AppendLine("");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("        <script src=\"/Content/jquery/jquery-1.9.1.min.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <!--[if gte IE 9]><!-->");
            sb.AppendLine("    <script src=\"~/Content/jquery/jquery.min.js\"></script>");
            sb.AppendLine("    <!--<![endif]-->");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("    <script src=\"https://cdn.staticfile.org/html5shiv/r29/html5.min.js\"></script>");
            sb.AppendLine("    <script src=\"https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <script src=\"~/Content/layui/layui.js\"></script>");
            sb.AppendLine("    <script src=\"~/Content/select2/js/select2.min.js\"></script>");
            sb.AppendLine("    <script src=\"~/Content/framework/js/global.js\"></script>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("    <form id=\"form\" class=\"layui-form\" style=\"margin-top: 25px\">");

            //重组数据，两两一组
            List<List<CodeGenerator>> listList = new List<List<CodeGenerator>>();
            List<CodeGenerator> list = forms.Where(it => it.IsPrimaryKey != "1" && it.PropertyName != "CreateUser"
            && it.PropertyName != "CreateTime" && it.PropertyName != "UpdateUser"
            && it.PropertyName != "UpdateTime" && it.PropertyName != "ModifyTime" && it.PropertyName != "ModifyUser").ToList();
            int count = list.Count;
            //可以分成多少组
            int group = 0;
            if (count % 2 == 0)
            {
                group = count / 2;
            }
            else
            {
                group = count / 2 + 1;
            }
            for (int i = 0; i < group; i++)
            {
                List<CodeGenerator> codeList = new List<CodeGenerator>();
                codeList.Add(list[i * 2]);
                if ((i * 2 + 1) < list.Count)
                {
                    codeList.Add(list[i * 2 + 1]);
                }
                listList.Add(codeList);
            }
            bool first = true;
            foreach (List<CodeGenerator> codeList in listList)
            {
                sb.AppendLine("        <div class=\"layui-form-item\">");
                foreach (CodeGenerator code in codeList)
                {
                    sb.AppendLine("            <div class=\"layui-inline\">");
                    sb.AppendLine($"                <label class=\"layui-form-label label-required\">{code.Other}</label>");
                    sb.AppendLine("                <div class=\"layui-input-inline\">");
                    if (first)
                    {
                        sb.AppendLine($"                    <input type=\"hidden\" name=\"{primaryKeyPropertyName}\" />");
                        first = false;
                    }
                    sb.AppendLine($"                    <input type=\"text\" name=\"{code.PropertyName}\" id=\"{code.PropertyName}\" lay-verify=\"required\" placeholder=\"请输入{code.Other}\" autocomplete=\"off\" class=\"layui-input\">");
                    sb.AppendLine("                </div>");
                    sb.AppendLine("            </div>");
                };
                sb.AppendLine("        </div>");
            }
            sb.AppendLine("        <div class=\"layui-form-item\" style=\"display: none\">");
            sb.AppendLine("            <div class=\"layui-input-block\">");
            sb.AppendLine("                <button id=\"btnSubmit\" class=\"layui-btn\" lay-submit lay-filter=\"add\">提交</button>");
            sb.AppendLine("            </div>");
            sb.AppendLine("        </div>");
            sb.AppendLine("    </form>");
            sb.AppendLine("");
            sb.AppendLine("    <script src=\"~/Content/jquery/jquery.md5.js\"></script>");
            sb.AppendLine("    <script>");
            sb.AppendLine("        layui.use(['element', 'form', 'layer'], function () {");
            sb.AppendLine("            var form = layui.form;");
            sb.AppendLine("            var element = layui.element;");
            sb.AppendLine("");
            sb.AppendLine("            var primaryKey = $.getQueryString(\"primaryKey\");");
            sb.AppendLine("            if (primaryKey) {");
            sb.AppendLine("                $.ajax({");
            sb.AppendLine($"                    url: \"/{area}/{className}/getForm\",");
            sb.AppendLine("                    data: { primaryKey: primaryKey },");
            sb.AppendLine("                    type: \"post\",");
            sb.AppendLine("                    dataType: \"json\",");
            sb.AppendLine("                    async: false,");
            sb.AppendLine("                    success: function (data) {");
            sb.AppendLine("                        $(\"#form\").formSerialize(data);");
            sb.AppendLine("                    }");
            sb.AppendLine("                });");
            sb.AppendLine("            }");
            sb.AppendLine("");
            sb.AppendLine("            form.render();");
            sb.AppendLine("");
            sb.AppendLine("            form.on('submit(add)', function (form) {");
            sb.AppendLine("                $.formSubmit({");
            sb.AppendLine($"                    url:\"/{area}/{className}/form\",");
            sb.AppendLine("                    data: form.field");
            sb.AppendLine("                });");
            sb.AppendLine("                return false;");
            sb.AppendLine("            });");
            sb.AppendLine("");
            sb.AppendLine("        });");
            sb.AppendLine("    </script>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");


            return sb.ToString();
        }


        /// <summary>
        /// 生成Index页面
        /// </summary>
        /// <param name="areas"></param>
        /// <param name="forms"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private string GenerateViewIndex(string area, List<CodeGenerator> forms, string primaryKeyPropertyName)
        {
            StringBuilder sb = new StringBuilder();
            string className = forms[0].ClassName;
            sb.AppendLine("@{");
            sb.AppendLine("    Layout = null;");
            sb.AppendLine("}");
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("    <meta name=\"viewport\" content=\"width=device-width\" />");
            sb.AppendLine($"    <title>{className}列表</title>");
            sb.AppendLine("    <link href=\"~/Content/layui/css/layui.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/framework/css/console.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/framework/css/animate.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <link href=\"~/Content/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" />");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("        <script src=\"/Content/jquery/jquery-1.9.1.min.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <!--[if gte IE 9]><!-->");
            sb.AppendLine("    <script src=\"~/Content/jquery/jquery.min.js\"></script>");
            sb.AppendLine("    <!--<![endif]-->");
            sb.AppendLine("    <!--[if lt IE 9]>");
            sb.AppendLine("    <script src=\"https://cdn.staticfile.org/html5shiv/r29/html5.min.js\"></script>");
            sb.AppendLine("    <script src=\"https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js\"></script>");
            sb.AppendLine("    <![endif]-->");
            sb.AppendLine("    <script src=\"~/Content/layui/layui.js\"></script>");
            sb.AppendLine("    <script src=\"~/Content/framework/js/global.js\"></script>");
            sb.AppendLine("</head>");
            sb.AppendLine("<body>");
            sb.AppendLine("    <div class=\"panel animated fadeIn\">");
            sb.AppendLine("        <div class=\"panel-body\">");
            sb.AppendLine("            <div id=\"toolbar\" class=\"elight-table-toolbar\">");
            sb.AppendLine("                <div class=\"layui-btn-group\"></div>");
            sb.AppendLine("                <button id=\"btnSearch\" class=\"toolbar-search-button layui-btn layui-btn-normal layui-btn-small\" style=\"line-height:20px\">");
            sb.AppendLine("                    <i class=\"layui-icon\">&#xe615;</i>");
            sb.AppendLine("                </button>");
            sb.AppendLine("                <div class=\"toolbar-search-input\">");
            sb.AppendLine($"                    <input type=\"text\" id=\"keyWord\" placeholder=\"{primaryKeyPropertyName}\" autocomplete=\"off\" class=\"layui-input\" style=\"height:39px\">");
            sb.AppendLine("                </div>");
            sb.AppendLine("            </div>");
            sb.AppendLine("            <table id=\"gridList\" class=\"layui-form layui-table table-hover elight-table\" lay-skin=\"line\">");
            sb.AppendLine("                <thead>");
            sb.AppendLine("                    <tr>");
            sb.AppendLine("                        <th>");
            sb.AppendLine("                            <input type=\"checkbox\" lay-skin=\"primary\"></th>");
            foreach (CodeGenerator code in forms)
            {
                if (code.IsPrimaryKey == "1")
                    continue;
                if (code.PropertyName == "CreateUser")
                    continue;
                if (code.PropertyName == "CreateTime")
                    continue;
                if (code.PropertyName == "UpdateUser")
                    continue;
                if (code.PropertyName == "UpdateTime")
                    continue;
                if (code.PropertyName == "ModifyUser")
                    continue;
                if (code.PropertyName == "ModifyTime")
                    continue;
                sb.AppendLine($"                        <th>{code.Other}</th>");
            }
            sb.AppendLine("                    </tr>");
            sb.AppendLine("                </thead>");
            sb.AppendLine("                <!--内容容器-->");
            sb.AppendLine("                <tbody id=\"content\"></tbody>");
            sb.AppendLine("            </table>");
            sb.AppendLine("            <div id=\"paged\"></div>");
            sb.AppendLine("        </div>");
            sb.AppendLine("    </div>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");
            sb.AppendLine("");
            sb.AppendLine("<!--内容模板-->");
            sb.AppendLine("<script id=\"contentTpl\" type=\"text/html\">");
            sb.AppendLine("    {{#  layui.each(d.list, function(index, item){ }}");
            sb.AppendLine("  <tr>");
            sb.AppendLine("      <td>");
            sb.AppendLine("          <input type=\"checkbox\" lay-skin=\"primary\" value=\"{{item." + primaryKeyPropertyName + "}}\"></td>");
            int count = 0;
            foreach (CodeGenerator code in forms)
            {
                if (code.IsPrimaryKey == "1")
                    continue;
                if (code.PropertyName == "CreateUser")
                    continue;
                if (code.PropertyName == "CreateTime")
                    continue;
                if (code.PropertyName == "UpdateUser")
                    continue;
                if (code.PropertyName == "UpdateTime")
                    continue;
                if (code.PropertyName == "ModifyUser")
                    continue;
                if (code.PropertyName == "ModifyTime")
                    continue;
                sb.AppendLine("      <td>{{item." + code.PropertyName + "}}</td>");
                count++;
            }
            sb.AppendLine("  </tr>");
            sb.AppendLine("    {{#  }); }}");
            sb.AppendLine("     {{# if(d.list.length<=0) { }}");
            sb.AppendLine("        <tr style=\"color:red\">");
            sb.AppendLine($"            <td colspan=\"{count}\">查无数据。</td>");
            sb.AppendLine("        </tr>");
            sb.AppendLine("    {{# } }}");
            sb.AppendLine("</script>");
            sb.AppendLine("");
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine("    var paging;");
            sb.AppendLine("    layui.config({");
            sb.AppendLine("        base: parent._baseUrl");
            sb.AppendLine("    }).use(['paging', 'form', 'layer'], function () {");
            sb.AppendLine("        var layer = parent.layer || layui.layer;");
            sb.AppendLine("        var form = layui.form;");
            sb.AppendLine("        paging = layui.paging();");
            sb.AppendLine("        initGrid();");
            sb.AppendLine("        $(\"#toolbar\").authorizeButton();");
            sb.AppendLine("        $(\"#btnSearch\").click(initGrid);");
            sb.AppendLine("        $(\"#keyWord\").bindEnterEvent(initGrid);");
            sb.AppendLine("    });");
            sb.AppendLine("");
            sb.AppendLine("    function initGrid() {");
            sb.AppendLine("        paging.init({");
            sb.AppendLine($"            url: '/{area}/{className}/index',");
            sb.AppendLine("            elem: '#content',");
            sb.AppendLine("            tempElem: '#contentTpl',");
            sb.AppendLine("            params: {");
            sb.AppendLine("                keyWord: $(\"#keyWord\").val()");
            sb.AppendLine("            },");
            sb.AppendLine("            pageConfig: {");
            sb.AppendLine("                elem: 'paged',");
            sb.AppendLine("                pageSize: 10,");
            sb.AppendLine("            },");
            sb.AppendLine("            success: function () {");
            sb.AppendLine("");
            sb.AppendLine("            }");
            sb.AppendLine("        });");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("    function btn_add() {");
            sb.AppendLine("        $.layerOpen({");
            sb.AppendLine("            id: \"add\",");
            sb.AppendLine($"            title: \"新增{className}\",");
            sb.AppendLine("            width: \"670px\",");
            sb.AppendLine("            height: \"530px\",");
            sb.AppendLine($"            content: \"/{area}/{className}/form\",");
            sb.AppendLine("            yes: function (iBody) {");
            sb.AppendLine("                iBody.find('#btnSubmit').click();");
            sb.AppendLine("                initGrid();");
            sb.AppendLine("            }");
            sb.AppendLine("        });");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("    function btn_edit() {");
            sb.AppendLine("        var ids = $(\"#gridList\").gridSelectedRowValue();");
            sb.AppendLine("        if (ids.length != 1) {");
            sb.AppendLine("            $.layerMsg(\"请勾选单条记录修改。\", \"warning\");");
            sb.AppendLine("            return;");
            sb.AppendLine("        }");
            sb.AppendLine("        $.layerOpen({");
            sb.AppendLine("            id: \"edit\",");
            sb.AppendLine($"            title: \"修改{className}\",");
            sb.AppendLine("            width: \"670px\",");
            sb.AppendLine("            height: \"530px\",");
            sb.AppendLine($"            content: \"/{area}/{className}/form?primaryKey=\" + ids[0],");
            sb.AppendLine("            yes: function (iBody) {");
            sb.AppendLine("                iBody.find('#btnSubmit').click();");
            sb.AppendLine("                initGrid();");
            sb.AppendLine("            }");
            sb.AppendLine("        });");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("    function btn_delete() {");
            sb.AppendLine("        var ids = $(\"#gridList\").gridSelectedRowValue();");
            sb.AppendLine("        if (ids.length < 1) {");
            sb.AppendLine("            $.layerMsg(\"请勾选需要删除的记录。\", \"warning\");");
            sb.AppendLine("            return;");
            sb.AppendLine("        }");
            sb.AppendLine("        $.layerConfirm({");
            sb.AppendLine("            content: \"您已选中\" + ids.length + \"条数据, 确定删除吗？\",");
            sb.AppendLine("            callback: function () {");
            sb.AppendLine("                $.formSubmit({");
            sb.AppendLine($"                    url: '/{area}/{className}/delete',");
            sb.AppendLine("                    data: { primaryKey: ids.join() },");
            sb.AppendLine("                    success: function () {");
            sb.AppendLine("                        initGrid();");
            sb.AppendLine("                    }");
            sb.AppendLine("                });");
            sb.AppendLine("            }");
            sb.AppendLine("        });");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("    function btn_detail() {");
            sb.AppendLine("        var ids = $(\"#gridList\").gridSelectedRowValue();");
            sb.AppendLine("        if (ids.length != 1) {");
            sb.AppendLine("            $.layerMsg(\"请勾选单条记录查看。\", \"warning\");");
            sb.AppendLine("            return;");
            sb.AppendLine("        }");
            sb.AppendLine("        $.layerOpen({");
            sb.AppendLine("            id: \"detail\",");
            sb.AppendLine($"            title: \"查看{className}\",");
            sb.AppendLine("            width: \"670px\",");
            sb.AppendLine("            height: \"530px\",");
            sb.AppendLine($"            content: \"/{area}/{className}/detail?primaryKey=\" + ids[0],");
            sb.AppendLine("            btn: null");
            sb.AppendLine("        });");
            sb.AppendLine("    }");
            sb.AppendLine("");
            sb.AppendLine("</script>");
            return sb.ToString();
        }

        /// <summary>
        /// 生成Controller
        /// </summary>
        /// <param name="areas"></param>
        /// <param name="forms"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        private string GenerateController(string area, List<CodeGenerator> forms, string primaryKeyProperty)
        {
            string className = forms[0].ClassName;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"using System.Collections.Generic;");
            sb.AppendLine($"using Microsoft.AspNetCore.Mvc;");
            sb.AppendLine($"using Elight.WebUI.Filters;");
            sb.AppendLine($"using Elight.Logic.{area};");
            sb.AppendLine($"using Elight.Entity.{area};");
            sb.AppendLine($"using Elight.Utility.ResponseModels;");
            sb.AppendLine($"using Elight.Utility.Core;");
            sb.AppendLine($"using Elight.Utility.Operator;");
            sb.AppendLine($"using Elight.WebUI.Controllers;");

            sb.AppendLine($"namespace Elight.WebUI.Areas.{area}.Controllers");
            sb.AppendLine("{");
            sb.AppendLine($"     [HiddenApi]");
            sb.AppendLine($"     [Area(\"{area}\")]");
            sb.AppendLine($"     public class {className}Controller : BaseController");
            sb.AppendLine("     {");
            sb.AppendLine($"        private {className}Logic logic;");

            sb.AppendLine($"        /// <summary>");
            sb.AppendLine($"        /// 构造方法");
            sb.AppendLine($"        /// </summary>");
            sb.AppendLine($"        public {className}Controller()");
            sb.AppendLine("        {");
            sb.AppendLine($"            logic = new {className}Logic();");
            sb.AppendLine("        }");



            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 主界面");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"       [HttpGet, Route(\"{area}/{className}/index\"), AuthorizeChecked]");
            sb.AppendLine("        public ActionResult Index()");
            sb.AppendLine("        {");
            sb.AppendLine("           return View();");
            sb.AppendLine("        }");
            sb.AppendLine("");



            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 主界面条件检索数据");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"pageIndex\"></param>");
            sb.AppendLine("        /// <param name=\"pageSize\"></param>");
            sb.AppendLine("        /// <param name=\"keyWord\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        [HttpPost, Route(\"{area}/{className}/index\"), AuthorizeChecked]");
            sb.AppendLine("        public ActionResult Index(int pageIndex, int pageSize, string keyWord)");
            sb.AppendLine("        {");
            sb.AppendLine("            int totalCount = 0;");
            sb.AppendLine("            var pageData = logic.GetList(pageIndex, pageSize, keyWord, ref totalCount);");
            sb.AppendLine($"            var result = new LayPadding<{className}>()");
            sb.AppendLine("            {");
            sb.AppendLine("                result = true,");
            sb.AppendLine("                msg = \"success\",");
            sb.AppendLine("                list = pageData,");
            sb.AppendLine("                count = totalCount//pageData.Count,");
            sb.AppendLine("            };");
            sb.AppendLine("            return Content(result.ToJson());");
            sb.AppendLine("        }");

            sb.AppendLine("");
            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 新增编辑页面");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        [HttpGet,Route(\"{area}/{className}/form\"),AuthorizeChecked]");
            sb.AppendLine("        public ActionResult Form()");
            sb.AppendLine("        {");
            sb.AppendLine("            return View();");
            sb.AppendLine("        }");
            sb.AppendLine("");

            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 新增编辑页面数据提交");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"model\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        [HttpPost, Route(\"{area}/{className}/form\"), AuthorizeChecked]");
            sb.AppendLine($"        public ActionResult Form({className} model)");
            sb.AppendLine("        {");
            sb.AppendLine($"            if (model.{primaryKeyProperty}.IsNullOrEmpty())");
            sb.AppendLine("            {");
            sb.AppendLine("                int row = logic.Insert(model);");
            sb.AppendLine("                return row > 0 ? Success() : Error();");
            sb.AppendLine("            }");
            sb.AppendLine("            else");
            sb.AppendLine("            {");
            sb.AppendLine("                int row = logic.Update(model);");
            sb.AppendLine("                return row > 0 ? Success() : Error();");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("");

            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 详细页面");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        [HttpGet, Route(\"{area}/{className}/detail\"), AuthorizeChecked]");
            sb.AppendLine("        public ActionResult Detail()");
            sb.AppendLine("        {");
            sb.AppendLine("            return View();");
            sb.AppendLine("        }");
            sb.AppendLine("");
            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 删除数据");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"primaryKey\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        [HttpPost, Route(\"{area}/{className}/delete\"), AuthorizeChecked]");
            sb.AppendLine("        public ActionResult Delete(string primaryKey)");
            sb.AppendLine("        {");
            sb.AppendLine("            int row = logic.Delete(primaryKey);");
            sb.AppendLine("            return row > 0 ? Success() : Error();");
            sb.AppendLine("        }");
            sb.AppendLine("");

            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 主键查询");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"primaryKey\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        [HttpPost,Route(\"{area}/{className}/getForm\"), LoginChecked]");
            sb.AppendLine("        public ActionResult GetForm(string primaryKey)");
            sb.AppendLine("        {");
            sb.AppendLine($"            {className} entity = logic.Get(primaryKey);");
            sb.AppendLine("            return Content(entity.ToJson());");
            sb.AppendLine("        }");
            sb.AppendLine("     }");
            sb.AppendLine("}");
            return sb.ToString();
        }

        private string GenerateLogic(string area, List<CodeGenerator> forms, string primaryKeyProperty)
        {
            string className = forms[0].ClassName;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"using System;");
            sb.AppendLine($"using Elight.Entity.{area};");
            sb.AppendLine($"using Elight.Utility.Operator;");
            sb.AppendLine($"using Elight.Logic.Base;");
            sb.AppendLine($"using SqlSugar;");
            sb.AppendLine($"using System;");
            sb.AppendLine($"using System.Collections.Generic;");
            sb.AppendLine($"using System.Linq;");
            sb.AppendLine($"using Elight.Utility.Other;");
            sb.AppendLine($"using Elight.Utility.Core;");
            sb.AppendLine($"namespace Elight.Logic.{area}");
            sb.AppendLine("{");
            sb.AppendLine($"    public class {className}Logic : BaseLogic");
            sb.AppendLine("    {");
            sb.AppendLine($"        /// <summary>");
            sb.AppendLine($"        /// 获得所有的{className}信息");
            sb.AppendLine($"        /// </summary>");
            sb.AppendLine($"        /// <param name=\"pageIndex\"></param>");
            sb.AppendLine($"        /// <param name=\"pageSize\"></param>");
            sb.AppendLine($"        /// <param name=\"keyWord\"></param>");
            sb.AppendLine($"        /// <param name=\"totalCount\"></param>");
            sb.AppendLine($"        /// <returns></returns>");
            sb.AppendLine($"        public List<{className}> GetList(int pageIndex, int pageSize, string keyWord, ref int totalCount)");
            sb.AppendLine("        {");
            sb.AppendLine($"            using (var db = GetInstance())");
            sb.AppendLine("            {");
            sb.AppendLine($"                return db.Queryable<{className}>().ToPageList(pageIndex, pageSize,ref totalCount);");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("");
            sb.AppendLine($"        /// <summary>");
            sb.AppendLine($"        /// 根据关键字获取{className}信息");
            sb.AppendLine($"        /// </summary>");
            sb.AppendLine($"        /// <param name=\"primaryKey\"></param>");
            sb.AppendLine($"        /// <returns></returns>");
            sb.AppendLine($"        public {className} Get(string primaryKey)");
            sb.AppendLine("        {");
            sb.AppendLine("            using (var db = GetInstance())");
            sb.AppendLine("            {");
            sb.AppendLine($"                return db.Queryable<{className}>().Where(it=>it.{primaryKeyProperty} == primaryKey).First();");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("");

            sb.AppendLine("        /// <summary>");
            sb.AppendLine("        /// 根据关键字删除" + className + "信息");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"primaryKey\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine("        public int Delete(string primaryKey)");
            sb.AppendLine("        {");
            sb.AppendLine("            using (var db = GetInstance())");
            sb.AppendLine("            {");
            sb.AppendLine($"                return db.Deleteable<{className}>().Where(it=>it.{primaryKeyProperty} == primaryKey).ExecuteCommand();");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("");
            sb.AppendLine("        /// <summary>");
            sb.AppendLine($"        /// 新增{className}信息");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"model\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        public int Insert({className} model)");
            sb.AppendLine("        {");
            sb.AppendLine("            using (var db = GetInstance())");
            sb.AppendLine("            {");
            sb.AppendLine($"                model.{primaryKeyProperty} = UUID.StrSnowId;");

            foreach (CodeGenerator code in forms)
            {
                if (code.PropertyName == "CreateUser")
                {
                    sb.AppendLine("                model.CreateUser = OperatorProvider.Instance.Current.Account;");
                }
                if (code.PropertyName == "CreateTime")
                {
                    sb.AppendLine("                model.CreateTime = DateTime.Now;");
                }
                if (code.PropertyName == "UpdateUser")
                {
                    sb.AppendLine("                model.UpdateUser = OperatorProvider.Instance.Current.Account;");
                }
                if (code.PropertyName == "UpdateTime")
                {
                    sb.AppendLine("                model.UpdateTime = DateTime.Now;");
                }
                if (code.PropertyName == "ModifyUser")
                {
                    sb.AppendLine("                model.ModifyUser = OperatorProvider.Instance.Current.Account;");
                }
                if (code.PropertyName == "ModifyTime")
                {
                    sb.AppendLine("                model.ModifyTime = DateTime.Now;");
                }
            }
            sb.AppendLine($"                return db.Insertable<{className}>(model).ExecuteCommand();");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("");

            sb.AppendLine("        /// <summary>");
            sb.AppendLine($"        /// 更新{className}信息");
            sb.AppendLine("        /// </summary>");
            sb.AppendLine("        /// <param name=\"model\"></param>");
            sb.AppendLine("        /// <returns></returns>");
            sb.AppendLine($"        public int Update({className} model)");
            sb.AppendLine("        {");
            sb.AppendLine("            using (var db = GetInstance())");
            sb.AppendLine("            {");
            foreach (CodeGenerator code in forms)
            {
                if (code.PropertyName == "UpdateUser")
                {
                    sb.AppendLine("                model.UpdateUser = OperatorProvider.Instance.Current.Account;");
                }
                if (code.PropertyName == "UpdateTime")
                {
                    sb.AppendLine("                model.UpdateTime = DateTime.Now;");
                }
                if (code.PropertyName == "ModifyUser")
                {
                    sb.AppendLine("                model.ModifyUser = OperatorProvider.Instance.Current.Account;");
                }
                if (code.PropertyName == "ModifyTime")
                {
                    sb.AppendLine("                model.ModifyTime = DateTime.Now;");
                }
            }
            sb.AppendLine($"                return db.Updateable<{className}>(model).UpdateColumns(it => new");
            sb.AppendLine("                {");
            foreach (CodeGenerator attr in forms)
            {
                if (attr.IsPrimaryKey == "1")
                {
                    continue;
                }
                if (attr.PropertyName != "CreateUser" && attr.PropertyName != "CreateTime")
                {
                    sb.AppendLine($"                    it.{attr.PropertyName},");
                }
            }
            sb.AppendLine("                }).ExecuteCommand();");
            sb.AppendLine("            }");
            sb.AppendLine("        }");
            sb.AppendLine("");
            sb.AppendLine("    }");
            sb.AppendLine("}");
            return sb.ToString();
        }

        private string GenerateEntity(string area, string tableName, List<CodeGenerator> forms)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("using SqlSugar;");
            sb.AppendLine("using System; ");
            sb.AppendLine($"namespace Elight.Entity.{area}");
            sb.AppendLine("{");
            sb.AppendLine("    [SugarTable(\"" + tableName + "\")]");
            sb.AppendLine($"    public class {forms[0].ClassName}");
            sb.AppendLine("    {");
            foreach (CodeGenerator code in forms)
            {
                if (code.IsPrimaryKey == "1")
                {
                    sb.AppendLine($"        [SugarColumn(ColumnName = \"{code.ColumnName}\",ColumnDescription =\"{code.Other}\",IsPrimaryKey = true)]");
                }
                else
                {
                    sb.AppendLine($"        [SugarColumn(ColumnName = \"{code.ColumnName}\",ColumnDescription =\"{code.Other}\")]");
                }
                sb.AppendLine("        public " + code.CSType + " " + code.PropertyName + " { get; set; }");
                sb.AppendLine("");
            }
            sb.AppendLine("    }");
            sb.AppendLine("}");
            return sb.ToString();
        }

        private string GetCSharpTypeByDbTypeName(string dbTypeName)
        {
            if (dbTypeName.ToLower() == "varchar")
            {
                return "string";
            }
            if (dbTypeName.ToLower() == "text")
            {
                return "string";
            }
            if (dbTypeName.ToLower() == "nvarchar")
            {
                return "string";
            }
            if (dbTypeName.ToLower() == "float")
            {
                return "float";
            }
            if (dbTypeName.ToLower() == "int")
            {
                return "int";
            }
            if (dbTypeName.ToLower() == "integer")
            {
                return "int";
            }
            if (dbTypeName.ToLower() == "char")
            {
                return "string";
            }
            if (dbTypeName.ToLower() == "datetime")
            {
                return "DateTime?";
            }
            if (dbTypeName.ToLower() == "datetime2")
            {
                return "DateTime?";
            }
            if (dbTypeName.ToLower() == "real")
            {
                return "float";
            }
            if (dbTypeName.ToLower() == "double")
            {
                return "double";
            }
            if (dbTypeName.ToLower() == "decimal")
            {
                return "decimal";
            }
            return "";
        }

        private string GetPropertyNameByColumnName(string columnName)
        {
            //按_拆分，得到每个单词，再将每个单词的首字母大写，再拼起来
            string[] split = columnName.Split('_');
            string className = "";
            foreach (string str in split)
            {
                className += str.First().ToString().ToUpper() + str.Substring(1);
            }
            return className;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private string GetClassNameByTableName(string tableName)
        {
            //按_拆分，得到每个单词，再将每个单词的首字母大写，再拼起来
            string[] split = tableName.Split('_');
            string className = "";
            foreach (string str in split)
            {
                className += str.First().ToString().ToUpper() + str.Substring(1);
            }
            return className;
        }
    }
}
