﻿using Elight.Entity.GlobalData;
using Elight.Entity.Sys;
using Elight.Logic.Sys;
using Elight.Utility.Core;
using Elight.Utility.Extension;
using Elight.Utility.Operator;
using Elight.WebUI.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Elight.WebUI.Controllers
{
    [HiddenApi]
    public class IndexController : BaseController
    {
        [Route("ueditor.html")]
        [HttpGet]
        public ActionResult UEditor()
        {
            return View();
        }

        [Route("video.html")]
        [HttpGet]
        public ActionResult Video()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PrintTest()
        {
            return View();
        }

        /// <summary>
        /// 打开系统配置页面
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("config")]
        public ActionResult Config()
        {
            //获得配置文件所有属性
            List<object> list = new List<object>();
            //反射获得配置文件所有属性
            Type type = typeof(ServerConfigs);
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                string name = property.Name;
                object[] attr = property.GetCustomAttributes(false);
                if (attr.Length > 0)
                {
                    ConfigAttribute myattr = attr[0] as ConfigAttribute;
                    if (myattr != null)
                    {
                        name = myattr.Name;
                        //从注解数组中取第一个注解(一个属性可以包含多个注解) 
                        object item = new { Name = name, Key = property.Name, Value = property.GetValue(GlobalValue.Config).ToString() };
                        list.Add(item);
                    }
                }
            }
            ViewBag.list = list;
            return View();
        }

        [HttpPost, Route("config")]
        public ActionResult Config(ServerConfigs configs)
        {
            SetConfigValue(configs);
            string json = GlobalValue.Config.ToJson();
            json = json.FormatJson();
            System.IO.File.WriteAllText(MyEnvironment.RootPath("Configs/config.json"), json, Encoding.UTF8);
            return Success();
        }

        private void SetConfigValue(ServerConfigs configs)
        {
            //找到configs中所有的
            Type type = typeof(ServerConfigs);
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object[] attr = property.GetCustomAttributes(false);
                if (attr.Length > 0)
                {
                    ConfigAttribute myattr = attr[0] as ConfigAttribute;
                    if (myattr != null)
                    {
                        object value = property.GetValue(configs);
                        property.SetValue(GlobalValue.Config, value);
                    }
                }
            }
        }


        /// <summary>
        /// 主题修改页面显示
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("theme"), LoginChecked]
        public ActionResult Theme()
        {
            string userId = OperatorProvider.Instance.Current.UserId;
            SysUserLogOn userLogOn = new SysUserLogOnLogic().GetByAccount(userId);
            ViewBag.Theme = userLogOn.Theme;
            return View();
        }


        /// <summary>
        /// 主题修改显示
        /// </summary>
        /// <param name="theme"></param>
        /// <returns></returns>
        [HttpPost, Route("theme"), LoginChecked]
        public ActionResult Theme(string theme)
        {
            Operator user = OperatorProvider.Instance.Current;
            SysUserLogOn userLogOn = new SysUserLogOnLogic().GetByAccount(user.UserId);
            userLogOn.Theme = theme;
            int row = new SysUserLogOnLogic().UpdateTheme(userLogOn);
            if (row > 0)
            {
                user.Theme = theme;
                OperatorProvider.Instance.Current = user;
                return Success();
            }
            return Error();
        }
    }
}
