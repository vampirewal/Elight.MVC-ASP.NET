﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elight.Entity.GlobalData
{
    public class ConfigAttribute : Attribute
    {
        public ConfigAttribute()
        {

        }
        public ConfigAttribute(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}
