﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elight.Entity.GlobalData
{
    /// <summary>
    /// 全局数据存放
    /// </summary>
    public class GlobalValue
    {
        /// <summary>
        /// 全局配置信息
        /// </summary>
        public static ServerConfigs Config;

    }
}
